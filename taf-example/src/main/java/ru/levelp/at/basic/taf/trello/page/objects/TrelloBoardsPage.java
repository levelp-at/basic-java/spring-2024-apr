package ru.levelp.at.basic.taf.trello.page.objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelp.at.basic.taf.trello.dictionary.CreateBoardValue;
import ru.levelp.at.basic.taf.trello.service.webdriver.WebDriverSingleton;

public class TrelloBoardsPage extends AbstractBasePage {

    private static final By CREATE_BOARDS_ELEMENTS = By
        .xpath("//nav/ul/li/button[contains(@data-testid, 'header-create')]");

    @FindBy(xpath = "//*[@data-testid='header-create-menu-button']")
    private WebElement createButton;

    @FindBy(xpath = "//*[@data-testid='create-board-title-input']")
    private WebElement boardNameTextField;

    @FindBy(xpath = "//*[@data-testid='create-board-submit-button']")
    private WebElement createBoardButton;

    public TrelloBoardsPage() {
        super(WebDriverSingleton.getDriver());
    }

    @Override
    public void open() {
        throw new UnsupportedOperationException("Метод не реализован");
    }

    public void clickCreateButton() {
        wait.until(ExpectedConditions.elementToBeClickable(createButton)).click();
    }

    public void create(CreateBoardValue value) {
        final var list = wait.until(ExpectedConditions.numberOfElementsToBe(CREATE_BOARDS_ELEMENTS, 3));
        list.stream().filter(elem -> wait
                .until(ExpectedConditions.elementToBeClickable(elem)).getText().contains(value.getValue()))
            .findFirst().ifPresent(WebElement::click);
    }

    public void enterBoardName(final String boardName) {
        wait.until(ExpectedConditions.visibilityOf(boardNameTextField)).sendKeys(boardName);
    }

    public void clickCreateBoardButton() {
        wait.until(ExpectedConditions.elementToBeClickable(createBoardButton)).click();
    }
}
