package ru.levelp.at.basic.taf.trello.page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelp.at.basic.taf.trello.service.webdriver.WebDriverSingleton;

public class AtlassianLoginPage extends AbstractBasePage {

    @FindBy(xpath = "//*[@data-testid='username']")
    private WebElement usernameTextField;

    @FindBy(xpath = "//*[@id='login-submit']")
    private WebElement continueLoginButton;

    @FindBy(xpath = "//*[@data-testid='password']")
    private WebElement passwordTextField;

    public AtlassianLoginPage() {
        super(WebDriverSingleton.getDriver());
    }

    @Override
    public void open() {
        throw new UnsupportedOperationException("Метод не поддерживается");
    }

    public void enterUsername(final String username) {
        wait.until(ExpectedConditions.visibilityOf(usernameTextField))
            .sendKeys(username);
    }

    public void enterPassword(final String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordTextField))
            .sendKeys(password);
    }

    public void clickContinueLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(continueLoginButton)).click();
    }
}
