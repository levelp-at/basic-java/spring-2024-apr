package ru.levelp.at.basic.taf.trello.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.LoadPolicy;
import org.aeonbits.owner.Config.LoadType;
import org.aeonbits.owner.Config.Sources;

@LoadPolicy(LoadType.MERGE)
@Sources({
    "system:properties",
    "system:env",
    "classpath:env/${env}/ui-application.properties"
})
public interface UiConfiguration extends Config {

    @Key("trello.url")
    String url();

    @Key("trello.username")
    String username();

    @Key("trello.password")
    String password();
}
