package ru.levelp.at.basic.taf.trello.exception;

public class UnsupportedBrowserException extends RuntimeException {

    public UnsupportedBrowserException(String browserName) {
        super("Unsupported browser: %s".formatted(browserName));
    }
}
