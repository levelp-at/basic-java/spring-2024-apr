package ru.levelp.at.basic.taf.trello.dictionary;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public enum CreateBoardValue {

    CREATE_BOARD("Create board"),
    START_WITH_A_TEMPLATE("Start with a template"),
    CREATE_WORKSPACE("Create workspace");

    private final String value;
}
