package ru.levelp.at.basic.taf.trello.page.objects;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.levelp.at.basic.taf.trello.config.ConfigProvider;

public abstract class AbstractBasePage {

    private static final String SITE_URL;

    static {
        SITE_URL = ConfigProvider.uiConfiguration().url();
    }

    protected final WebDriver driver;
    protected final WebDriverWait wait;

    protected AbstractBasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(10000));

        PageFactory.initElements(driver, this);
    }

    protected void open(String relativeUrl) {
        driver.navigate().to("%s/%s".formatted(SITE_URL, relativeUrl));
    }

    public abstract void open();
}
