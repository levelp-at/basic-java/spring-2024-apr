package ru.levelp.at.basic.taf.trello.service.webdriver;

import org.openqa.selenium.WebDriver;

public final class WebDriverSingleton {

    private static WebDriver driver;

    private WebDriverSingleton() {

    }

    public static WebDriver getDriver() {
        if (driver == null) {
            var browserName = System.getProperty("browser.name", "chrome");
            driver = WebDriverFactory.create(browserName);
        }
        return driver;
    }

    public static void close() {
        driver.quit();
        driver = null;
    }
}
