package ru.levelp.at.basic.taf.trello.page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelp.at.basic.taf.trello.service.webdriver.WebDriverSingleton;

public class TrelloBoardPage extends AbstractBasePage {

    @FindBy(xpath = "//*[@data-testid='board-name-display']")
    private WebElement displayName;

    public TrelloBoardPage() {
        super(WebDriverSingleton.getDriver());
    }

    @Override
    public void open() {
        throw new UnsupportedOperationException("Метод не реализован");
    }

    public String getDisplayName() {
        return wait.until(ExpectedConditions.visibilityOf(displayName)).getText();
    }
}
