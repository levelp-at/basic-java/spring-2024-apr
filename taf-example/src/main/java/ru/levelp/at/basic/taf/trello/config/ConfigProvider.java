package ru.levelp.at.basic.taf.trello.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.aeonbits.owner.ConfigCache;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConfigProvider {

    public static UiConfiguration uiConfiguration() {
        return ConfigCache.getOrCreate(UiConfiguration.class);
    }
}
