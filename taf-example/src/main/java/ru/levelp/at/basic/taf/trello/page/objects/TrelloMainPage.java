package ru.levelp.at.basic.taf.trello.page.objects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelp.at.basic.taf.trello.service.webdriver.WebDriverSingleton;

public class TrelloMainPage extends AbstractBasePage {

    @FindBy(xpath = "//a[contains(@data-uuid, '_login')]")
    private WebElement loginButton;

    public TrelloMainPage() {
        super(WebDriverSingleton.getDriver());
    }

    @Override
    public void open() {
        open("/");
    }

    public void clickLoginButton() {
        wait.until(ExpectedConditions.elementToBeClickable(loginButton)).click();
    }
}
