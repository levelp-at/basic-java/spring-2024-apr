package ru.levelp.at.basic.taf.trello.test.board;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.levelp.at.basic.taf.trello.config.ConfigProvider;
import ru.levelp.at.basic.taf.trello.step.BoardTrelloStep;
import ru.levelp.at.basic.taf.trello.step.CommonTrelloStep;
import ru.levelp.at.basic.taf.trello.test.AbstractBaseTest;

@DisplayName("Тесты на страницу доска")
class BoardTest extends AbstractBaseTest {

    private static final Faker FAKER = new Faker();

    @Test
    @DisplayName("Создание новой доски")
    void createBoardTest() {
        final var boardName = FAKER.funnyName().name();
        final var config = ConfigProvider.uiConfiguration();

        CommonTrelloStep.openTrello();
        CommonTrelloStep.loginToApplication(config.username(), config.password());
        BoardTrelloStep.createBoard(boardName);

        final var actualBoardName = BoardTrelloStep.getBoardName();

        assertThat(actualBoardName)
            .as("Проверка создания доски с указанным именем")
            .isEqualTo(boardName);
    }
}
