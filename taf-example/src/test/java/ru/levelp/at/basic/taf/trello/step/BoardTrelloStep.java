package ru.levelp.at.basic.taf.trello.step;

import io.qameta.allure.Step;
import ru.levelp.at.basic.taf.trello.dictionary.CreateBoardValue;
import ru.levelp.at.basic.taf.trello.page.objects.TrelloBoardPage;
import ru.levelp.at.basic.taf.trello.page.objects.TrelloBoardsPage;

public class BoardTrelloStep {

    @Step("Создание доски")
    public static void createBoard(final String boardName) {
        final var boardsPage = new TrelloBoardsPage();
        boardsPage.clickCreateButton();
        boardsPage.create(CreateBoardValue.CREATE_BOARD);
        boardsPage.enterBoardName(boardName);
        boardsPage.clickCreateBoardButton();
    }

    public static String getBoardName() {
        return new TrelloBoardPage().getDisplayName();
    }
}
