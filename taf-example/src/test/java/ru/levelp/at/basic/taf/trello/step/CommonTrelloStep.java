package ru.levelp.at.basic.taf.trello.step;

import io.qameta.allure.Param;
import io.qameta.allure.Step;
import io.qameta.allure.model.Parameter.Mode;
import ru.levelp.at.basic.taf.trello.page.objects.AtlassianLoginPage;
import ru.levelp.at.basic.taf.trello.page.objects.TrelloMainPage;

public class CommonTrelloStep {

    @Step("Открываем trello.com")
    public static void openTrello() {
        new TrelloMainPage().open();
    }

    @Step("Логинимся под пользователем в приложение")
    public static void loginToApplication(final String username,
                                          @Param(mode = Mode.MASKED) final String password) {
        // new TrelloMainPage().clickLoginButton();
        final var loginPage = new AtlassianLoginPage();
        loginPage.enterUsername(username);
        loginPage.clickContinueLoginButton();
        loginPage.enterPassword(password);
        loginPage.clickContinueLoginButton();
    }
}
