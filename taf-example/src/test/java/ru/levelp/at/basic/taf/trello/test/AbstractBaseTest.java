package ru.levelp.at.basic.taf.trello.test;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.levelp.at.basic.taf.trello.extension.AllureAttachmentExtension;
import ru.levelp.at.basic.taf.trello.service.webdriver.WebDriverSingleton;

@ExtendWith(AllureAttachmentExtension.class)
public abstract class AbstractBaseTest {

    @BeforeEach
    void setUp() {
        WebDriverSingleton.getDriver();
    }

    @AfterEach
    void tearDown() {
        WebDriverSingleton.close();
    }
}
