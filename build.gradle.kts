plugins {
    `java-library`
    checkstyle
}

repositories {
    mavenLocal()
    maven {
        url = uri("https://repo.maven.apache.org/maven2/")
    }
}

group = "ru.levelp.at.basic"
version = "1.0-SNAPSHOT"
description = "spring-2024-apr"
java.sourceCompatibility = JavaVersion.VERSION_17
java.targetCompatibility = JavaVersion.VERSION_17

checkstyle {
    toolVersion = "10.12.4"
}

dependencies {
//    api(libs.org.apache.commons.commons.collections4)
//    api(libs.org.apache.commons.commons.lang3)
    checkstyle("com.puppycrawl.tools:checkstyle:${checkstyle.toolVersion}")

    api(libs.bundles.apache.commons)
}

tasks.withType<JavaCompile>() {
    options.encoding = "UTF-8"
}

tasks.withType<Javadoc>() {
    options.encoding = "UTF-8"
}

tasks.withType<Checkstyle>().configureEach {
    reports {
        xml.required = false
        html.required = true
    }
}
