package ru.levelp.at.basic.lesson13.bdd.usersbugred;

import static io.cucumber.junit.platform.engine.Constants.GLUE_PROPERTY_NAME;

import org.junit.platform.suite.api.ConfigurationParameter;
import org.junit.platform.suite.api.IncludeEngines;
import org.junit.platform.suite.api.SelectClasspathResource;
import org.junit.platform.suite.api.Suite;

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("ru/levelp/at/basic/lesson13/bdd/usersbugred")
@ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "ru.levelp.at.basic.lesson13.bdd.usersbugred")
public class RunCucumberTest {
}
