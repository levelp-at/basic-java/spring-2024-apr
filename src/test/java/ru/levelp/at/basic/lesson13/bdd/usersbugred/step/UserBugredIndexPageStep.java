package ru.levelp.at.basic.lesson13.bdd.usersbugred.step;

import static org.assertj.core.api.Assertions.assertThat;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import ru.levelp.at.basic.lesson13.bdd.service.webdriver.WebDriverSingleton;
import ru.levelp.at.basic.lesson13.bdd.usersbugred.UsersBugredIndexPage;

public class UserBugredIndexPageStep {

    @Given("I open usersbugred web-site")
    public void openUsersBugredMainPage() {
        new UsersBugredIndexPage(WebDriverSingleton.getDriver()).open();
    }

    @Given("I navigate to Login Registration page")
    public void navigateToLoginRegistrationPage() {
        new UsersBugredIndexPage(WebDriverSingleton.getDriver()).clickEnterButton();
    }

    @Then("username in user dropdown should be equal {string} on Main page")
    public void usernameInUserDropdownShouldBeEqualOnMainPage(String expectedUsername) {
        var actualUsername = new UsersBugredIndexPage(WebDriverSingleton.getDriver()).getUserLabelText();

        assertThat(actualUsername)
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(expectedUsername);
    }
}
