package ru.levelp.at.basic.lesson13.bdd.usersbugred.step;

import io.cucumber.java.en.When;
import java.util.Map;
import ru.levelp.at.basic.lesson13.bdd.service.webdriver.WebDriverSingleton;
import ru.levelp.at.basic.lesson13.bdd.usersbugred.UsersBugredLoginRegistrationPage;

public class UsersBugredLoginRegistrationPageStep {

    @When("I enter username {string} in username text field in Registration block on Login Registration page")
    public void enterUsernameInUsernameTextFieldInRegistrationBlockOnLoginRegistrationPage(String username) {
        new UsersBugredLoginRegistrationPage(WebDriverSingleton.getDriver()).sendKeysToNameTextField(username);
    }

    @When("I enter email {string} in email text field in Registration block on Login Registration page")
    public void enterEmailInEmailTextFieldInRegistrationBlockOnLoginRegistrationPage(String email) {
        new UsersBugredLoginRegistrationPage(WebDriverSingleton.getDriver()).sendKeysToEmailTextField(email);
    }

    @When("I enter password {string} in password text field in Registration block on Login Registration page")
    public void enterPasswordInPasswordTextFieldInRegistrationBlockOnLoginRegistrationPage(String password) {
        new UsersBugredLoginRegistrationPage(WebDriverSingleton.getDriver()).sendKeysToPasswordTextField(password);
    }

    @When("I click on button \"Зарегистрироваться\" in Registration block on Login Registration page")
    public void clickOnButtonRegisterInRegistrationBlockOnLoginRegistrationPage() {
        new UsersBugredLoginRegistrationPage(WebDriverSingleton.getDriver()).clickRegisterButton();
    }

    @When("I register user in registration form in Registration block on Login Registration page with parameters")
    public void registerUserInRegistrationFormInRegistrationBlockOnLoginRegistrationPageWithParameters(
        Map<String, String> parameters) {
        var registrationPage = new UsersBugredLoginRegistrationPage(WebDriverSingleton.getDriver());
        registrationPage.sendKeysToNameTextField(parameters.get("username"));
        registrationPage.sendKeysToEmailTextField(parameters.get("email"));
        registrationPage.sendKeysToPasswordTextField(parameters.get("password"));
        registrationPage.clickRegisterButton();
    }
}
