package ru.levelp.at.basic.lesson13.bdd.usersbugred.hook;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import ru.levelp.at.basic.lesson13.bdd.service.webdriver.WebDriverSingleton;

public class WebDriverHook {

    @Before
    public void before() {
        WebDriverSingleton.getDriver();
    }

    @After
    public void after() {
        WebDriverSingleton.close();
    }
}
