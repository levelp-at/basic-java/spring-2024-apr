package ru.levelp.at.basic.lesson1011.allure.wildberries.step;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson0507.selenium.page.components.WildberriesProductsPage;
import ru.levelp.at.basic.lesson0507.selenium.page.components.elements.ProductCardElement;

public class WildberriesProductsPageStep {

    private final WebDriver driver;

    public WildberriesProductsPageStep(WebDriver driver) {
        this.driver = driver;
    }

    /**
     * Выбирает карточку продукта по указанному индексу на странице товаров.
     *
     * @param index Индекс карточки продукта, который нужно выбрать (нумерация с 1).
     * @return Элемент карточки продукта, соответствующий указанному индексу.
     */
    @Step("Выбор товара по индексу")
    public ProductCardElement selectCard(final int index) {
        var productsPage = new WildberriesProductsPage(driver);
        return productsPage.getProductCard(index);
    }
}
