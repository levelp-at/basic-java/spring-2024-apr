package ru.levelp.at.basic.lesson1011.cicd.extension;

import io.qameta.allure.Allure;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ExtensionContext.Namespace;
import org.junit.jupiter.api.extension.ExtensionContext.Store;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson1011.cicd.context.TestContext;

public class AllureAttachmentExtension implements BeforeEachCallback, AfterTestExecutionCallback {

    @Override
    public void beforeEach(ExtensionContext context) throws Exception {
        getStore(context);
    }

    @Override
    public void afterTestExecution(ExtensionContext context) throws Exception {
        System.out.println("sasasasssss");
        if (context.getExecutionException().isPresent()) {
            System.out.println("=====");
            // var driver = (WebDriver) getStore(context).get("driver");
            var driver = (WebDriver) TestContext.getInstance().get("driver");
            attachScreenshot(driver);
            attachPageSource(driver);
        }
    }

    @Attachment(value = "screenshot", type = "image/png", fileExtension = "png")
    private byte[] attachScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    private void attachPageSource(WebDriver driver) {
        Allure.addAttachment("page source", "text/html", driver.getPageSource(), ".html");
    }

    private Store getStore(ExtensionContext context) {
        return context.getStore(Namespace.create(getClass(), context.getRequiredTestMethod()));
    }
}
