package ru.levelp.at.basic.lesson1011.allure.tags.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.junit.jupiter.api.Tag;
import ru.levelp.at.basic.lesson1011.allure.tags.TagName;

@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Tag(TagName.NEGATIVE_TAG)
public @interface NegativeTag {
}
