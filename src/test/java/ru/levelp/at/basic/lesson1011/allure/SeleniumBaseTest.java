package ru.levelp.at.basic.lesson1011.allure;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.lesson1011.allure.extension.AllureAttachmentExtension;

@ExtendWith(AllureAttachmentExtension.class)
public abstract class SeleniumBaseTest {

    public static WebDriver driver;

    @BeforeEach
    protected void setUp() {
        Allure.step("Открытие браузера", () -> driver = new ChromeDriver());

    }

    @AfterEach
    protected void tearDown() {
        Allure.step("Закрытие браузера", () -> driver.quit());
    }
}
