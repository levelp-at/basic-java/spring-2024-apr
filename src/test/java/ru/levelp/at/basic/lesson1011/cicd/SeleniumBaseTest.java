package ru.levelp.at.basic.lesson1011.cicd;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import ru.levelp.at.basic.lesson1011.cicd.context.TestContext;
import ru.levelp.at.basic.lesson1011.cicd.extension.AllureAttachmentExtension;

@ExtendWith(AllureAttachmentExtension.class)
public abstract class SeleniumBaseTest {

    public WebDriver driver;

    @BeforeEach
    protected void setUp() {
        driver = Allure.step("Открытие браузера", () -> {
            var options = new ChromeOptions();
            var headless = Boolean.parseBoolean(System.getProperty("browser.headless"));

            if (headless) {
                options.addArguments("--headless=new", "--disable-dev-shm-usage", "--no-sandbox");
            }

            return new ChromeDriver(options);
        });
        TestContext.getInstance().put("driver", driver);
    }

    @AfterEach
    protected void tearDown() {
        Allure.step("Закрытие браузера", () -> driver.quit());
        TestContext.clearContext();
    }
}
