package ru.levelp.at.basic.lesson1011.allure.usersbugred.step;

import io.qameta.allure.Param;
import io.qameta.allure.Step;
import io.qameta.allure.model.Parameter.Mode;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.UsersBugredLoginRegistrationPage;

public class UsersBugredLoginRegistrationPageStep {

    private final WebDriver driver;

    public UsersBugredLoginRegistrationPageStep(WebDriver driver) {
        this.driver = driver;
    }

    @Step("Открываю страницу сайта")
    public void openPage() {
        new UsersBugredLoginRegistrationPage(driver).open();
    }

    @Step("Регистрация пользователя с именем: {name}, email: {email}")
    public void registerUser(String name, String email, @Param(mode = Mode.HIDDEN) String password) {
        var registerPage = new UsersBugredLoginRegistrationPage(driver);
        registerPage.sendKeysToNameTextField(name);
        registerPage.sendKeysToEmailTextField(email);
        registerPage.sendKeysToPasswordTextField(password);
        registerPage.clickRegisterButton();
    }

    public String getErrorMessageText() {
        return new UsersBugredLoginRegistrationPage(driver).getErrorMessageText();
    }
}
