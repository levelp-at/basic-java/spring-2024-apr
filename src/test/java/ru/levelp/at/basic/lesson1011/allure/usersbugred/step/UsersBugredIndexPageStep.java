package ru.levelp.at.basic.lesson1011.allure.usersbugred.step;

import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.UsersBugredIndexPage;

public class UsersBugredIndexPageStep {

    private final WebDriver driver;

    public UsersBugredIndexPageStep(WebDriver driver) {
        this.driver = driver;
    }

    public String getUserLabelText() {
        return new UsersBugredIndexPage(driver).getUserLabelText();
    }
}
