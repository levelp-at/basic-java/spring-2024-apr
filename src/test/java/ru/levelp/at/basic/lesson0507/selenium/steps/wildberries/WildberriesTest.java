package ru.levelp.at.basic.lesson0507.selenium.steps.wildberries;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.lesson0507.selenium.page.components.WildberriesProductPage;
import ru.levelp.at.basic.lesson0507.selenium.page.components.WildberriesProductsPage;
import ru.levelp.at.basic.lesson0507.selenium.steps.wildberries.step.WildberriesIndexPageStep;
import ru.levelp.at.basic.lesson0507.selenium.steps.wildberries.step.WildberriesProductPageStep;
import ru.levelp.at.basic.lesson0507.selenium.steps.wildberries.step.WildberriesProductsPageStep;
import ru.levelp.at.basic.utils.SleepUtil;

class WildberriesTest {

    private WebDriver driver;
    private WildberriesIndexPageStep indexPageStep;
    private WildberriesProductsPageStep productsPageStep;
    private WildberriesProductPageStep productPageStep;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        indexPageStep = new WildberriesIndexPageStep(driver);
        productsPageStep = new WildberriesProductsPageStep(driver);
        productPageStep = new WildberriesProductPageStep(driver);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }


    @Test
    void sampleCompositePageObjectTest() {
        indexPageStep.openPage();
        SleepUtil.sleep(3500);

        indexPageStep.selectProductCategory("Электроника", "Смартфоны и телефоны", "Смартфоны");
        SleepUtil.sleep(3500);

        var card = productsPageStep.selectCard(3);
        SleepUtil.sleep(3500);

        final var cardPriceWithoutAccount = card.getPriceWithoutWbAccount();
        final var cardPriceWithAccount = card.getPriceWithWbAccount();

        System.out.printf("price without account %s%n", cardPriceWithoutAccount);
        System.out.printf("price with account %s%n", cardPriceWithAccount);
        SleepUtil.sleep(3500);

        card.click();

        SleepUtil.sleep(3500);

        var productPage = new WildberriesProductPage(driver);
        final var actualPriceWithoutAccount = productPage.getPriceWithoutWbAccountLabel();
        final var actualPriceWithAccount = productPage.getPriceWithWbAccountLabel();

        assertThat(actualPriceWithoutAccount)
            .as("Провека цены без аккаунта WB")
            .isEqualTo(cardPriceWithoutAccount);

        assertThat(actualPriceWithAccount)
            .as("Провека цены c аккаунтом WB")
            .isEqualTo(cardPriceWithAccount);
    }
}
