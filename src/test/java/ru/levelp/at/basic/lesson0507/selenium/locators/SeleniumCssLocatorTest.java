package ru.levelp.at.basic.lesson0507.selenium.locators;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SeleniumCssLocatorTest {

    private static final String MAIL_RU_URL = "https://mail.ru";
    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void idLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.id("mailbox")); DOM locator
        // var element = driver.findElement(By.cssSelector("[id = 'mailbox']")); // CSS locator
        var element = driver.findElement(By.cssSelector("#mailbox")); // CSS locator
        System.out.println(element.getText());
    }

    @Test
    void classNameLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.className("mailbox")); DOM locator
        // var element = driver.findElement(By.cssSelector("[class = 'mailbox']")); // CSS locator
        var element = driver.findElement(By.cssSelector(".mailbox")); // CSS locator
        System.out.println(element.getText());
    }

    @Test
    void nameLocatorTest() {
        driver.navigate().to(GOOGLE_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.name("q")); DOM locator
        var element = driver.findElement(By.cssSelector("[name = 'q']")); // CSS locator
        element.sendKeys("selenium");

        SleepUtil.sleep(3000);
        // var elementButton = driver.findElement(By.name("btnK")); DOM locator
        var elementButton = driver.findElement(By.cssSelector("[name = 'btnK']")); // CSS locator
        elementButton.click();

        SleepUtil.sleep(1500);
    }

    @Test
    void tagNameLocatorTest() {
        driver.navigate().to(GOOGLE_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.name("q")); DOM locator
        var element = driver.findElement(By.cssSelector("[name = 'q']")); // CSS locator
        element.sendKeys("selenium");

        SleepUtil.sleep(3000);
        // var elementButton = driver.findElement(By.name("btnK")); DOM locator
        var elementButton = driver.findElement(By.cssSelector("[name = 'btnK']")); // CSS locator
        elementButton.click();

        SleepUtil.sleep(2000);
        // var elementLink = driver.findElements(By.tagName("a")); // DOM locator
        var elementLink = driver.findElements(By.cssSelector("a")); // CSS locator
        System.out.println(elementLink.size());
        elementLink.forEach(elem -> System.out.println(elem.getText()));

        SleepUtil.sleep(2000);
    }

    @Test
    void combineSearchLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.cssSelector("div#mailbox")); // CSS locator
        var element = driver.findElement(By.cssSelector("div[id = 'mailbox']")); // CSS locator
        System.out.println(element.getText());
    }

    @Test
    void combineSearchWithDownTreeLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        // var element = driver.findElement(By.cssSelector("div#mailbox button")); // CSS locator
        var element = driver.findElement(By.cssSelector("div[id = 'mailbox'] button")); // CSS locator
        System.out.println(element.getText());

        element.click();
        SleepUtil.sleep(2000);
    }

    @Test
    void anyAttributeLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.cssSelector("[aria-label = 'Все проекты']")); // CSS locator
        System.out.println(element.getText());

        element.click();
        SleepUtil.sleep(2000);
    }
}
