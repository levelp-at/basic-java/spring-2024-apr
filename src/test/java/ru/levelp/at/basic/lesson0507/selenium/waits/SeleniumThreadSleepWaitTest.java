package ru.levelp.at.basic.lesson0507.selenium.waits;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SeleniumThreadSleepWaitTest {

    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void threadSleepWaitTest() {
        final var startTime = System.currentTimeMillis();
        try {
            driver.navigate().to(GOOGLE_URL);
            SleepUtil.sleep(500);

            var element = driver.findElement(By.xpath("//*[@name = 'q']"));
            element.sendKeys("selenium");

            SleepUtil.sleep(3000);
            var elementButton = driver.findElement(By.xpath("//*[@name = 'btnK']"));
            elementButton.click();

            SleepUtil.sleep(4000);
            var elementLink = driver.findElement(By.xpath("//a/h3[1]"));
            System.out.println(elementLink.getText());
            elementLink.click();

            SleepUtil.sleep(2000);
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);
        }
    }
}
