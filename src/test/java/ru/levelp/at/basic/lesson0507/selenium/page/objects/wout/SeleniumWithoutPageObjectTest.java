package ru.levelp.at.basic.lesson0507.selenium.page.objects.wout;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class SeleniumWithoutPageObjectTest {

    private static final String USERS_BUGRED_URL = "http://users.bugred.ru/";
    private static final Faker FAKER = new Faker();

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(4));

        driver.navigate().to(USERS_BUGRED_URL);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void testRegister() {
        var enterButton = wait.until(ExpectedConditions.elementToBeClickable(By
            .xpath("//*[@id='main-menu']//li/a[contains(@href, '/login')]")));
        enterButton.click();

        final var name = FAKER.name().firstName();
        var nameTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By
            .xpath("//form[contains(@action, '/register/')]//input[@name='name']")));
        nameTextField.sendKeys(name);

        final var email = FAKER.internet().emailAddress();
        var emailTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By
            .xpath("//form[contains(@action, '/register/')]//input[@name='email']")));
        emailTextField.sendKeys(email);

        final var password = FAKER.internet().password();
        var passwordTextField = wait.until(ExpectedConditions.visibilityOfElementLocated(By
            .xpath("//form[contains(@action, '/register/')]//input[@name='password']")));
        passwordTextField.sendKeys(password);

        var registerButton = wait.until(ExpectedConditions.elementToBeClickable(By
            .xpath("//form[contains(@action, '/register/')]//input[@name='act_register_now']")));
        registerButton.click();

        var userLabel = wait.until(ExpectedConditions.visibilityOfElementLocated(By
            .xpath("//li[@id='fat-menu']/a[@class='dropdown-toggle']")));

        assertThat(userLabel.getText())
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }
}
