package ru.levelp.at.basic.lesson0507.selenium.waits;

import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class SeleniumExplicitWaitTest {

    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, Duration.ofSeconds(4));
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void explicitWaitTest() {
        final var startTime = System.currentTimeMillis();
        try {
            driver.navigate().to(GOOGLE_URL);

            var element = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name = 'q']")));
            element.sendKeys("selenium");

            var elementButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name = 'btnK']")));
            elementButton.click();

            var elementLinks = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.xpath("//a/h3"), 1));
            var elementLink = elementLinks.stream().findFirst().orElseThrow();
            System.out.println(elementLink.getText());
            elementLink.click();
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);
        }
    }
}
