package ru.levelp.at.basic.lesson0507.selenium.page.components;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class WildberriesTest {

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void sampleCompositePageObjectTest() {
        var indexPage = new WildberriesIndexPage(driver);
        indexPage.open();

        SleepUtil.sleep(3500);

        indexPage.header().openMenu();

        SleepUtil.sleep(5000);

        indexPage.header().menu().hoverMenuItem("Электроника");

        SleepUtil.sleep(3500);

        indexPage.header().menu().clickSubmenuItem("Смартфоны и телефоны");

        SleepUtil.sleep(3500);

        indexPage.header().menu().clickSubSubmenuItem("Смартфоны");

        SleepUtil.sleep(3500);

        var productsPage = new WildberriesProductsPage(driver);
        var card = productsPage.getProductCard(3);

        SleepUtil.sleep(3500);

        final var cardPriceWithoutAccount = card.getPriceWithoutWbAccount();
        final var cardPriceWithAccount = card.getPriceWithWbAccount();

        System.out.printf("price without account %s%n", cardPriceWithoutAccount);
        System.out.printf("price with account %s%n", cardPriceWithAccount);
        SleepUtil.sleep(3500);

        card.click();

        SleepUtil.sleep(3500);

        var productPage = new WildberriesProductPage(driver);
        final var actualPriceWithoutAccount = productPage.getPriceWithoutWbAccountLabel();
        final var actualPriceWithAccount = productPage.getPriceWithWbAccountLabel();

        assertThat(actualPriceWithoutAccount)
            .as("Провека цены без аккаунта WB")
            .isEqualTo(cardPriceWithoutAccount);

        assertThat(actualPriceWithAccount)
            .as("Провека цены c аккаунтом WB")
            .isEqualTo(cardPriceWithAccount);
    }
}
