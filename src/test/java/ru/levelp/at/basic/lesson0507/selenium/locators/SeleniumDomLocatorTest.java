package ru.levelp.at.basic.lesson0507.selenium.locators;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SeleniumDomLocatorTest {

    private static final String MAIL_RU_URL = "https://mail.ru";
    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void idLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.id("mailbox"));
        System.out.println(element.getText());
    }

    @Test
    void classNameLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.className("mailbox"));
        System.out.println(element.getText());
    }

    @Test
    void nameLocatorTest() {
        driver.navigate().to(GOOGLE_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.name("q"));
        element.sendKeys("selenium");

        SleepUtil.sleep(3000);
        var elementButton = driver.findElement(By.name("btnK"));
        elementButton.click();

        SleepUtil.sleep(1500);
    }

    @Test
    void linkTextLocatorTest() {
        driver.navigate().to(MAIL_RU_URL);
        SleepUtil.sleep(500);

        SleepUtil.sleep(2000);
        var elementLink = driver.findElement(By.linkText("Почта"));
        elementLink.click();

        SleepUtil.sleep(2000);
    }

    @Test
    void partialLinkTextLocatorTest() {
        driver.navigate().to(GOOGLE_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.name("q"));
        element.sendKeys("selenium");

        SleepUtil.sleep(3000);
        var elementButton = driver.findElement(By.name("btnK"));
        elementButton.click();

        SleepUtil.sleep(2000);
        var elementLink = driver.findElements(By.partialLinkText("elenium"));
        System.out.println(elementLink.size());
        elementLink.forEach(elem -> System.out.println(elem.getText()));

        SleepUtil.sleep(2000);
    }

    @Test
    void tagNameLocatorTest() {
        driver.navigate().to(GOOGLE_URL);
        SleepUtil.sleep(500);

        var element = driver.findElement(By.name("q"));
        element.sendKeys("selenium");

        SleepUtil.sleep(3000);
        var elementButton = driver.findElement(By.name("btnK"));
        elementButton.click();

        SleepUtil.sleep(2000);
        var elementLink = driver.findElements(By.tagName("a"));
        System.out.println(elementLink.size());
        elementLink.forEach(elem -> System.out.println(elem.getText()));

        SleepUtil.sleep(2000);
    }
}
