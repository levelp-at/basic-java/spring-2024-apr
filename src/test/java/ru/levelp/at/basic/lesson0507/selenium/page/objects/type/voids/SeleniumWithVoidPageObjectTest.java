package ru.levelp.at.basic.lesson0507.selenium.page.objects.type.voids;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class SeleniumWithVoidPageObjectTest {

    private static final Faker FAKER = new Faker();
    private static final String EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE
        = "register_not_correct_field (email)";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void testRegister() {
        var indexPage = new UsersBugredIndexPage(driver);
        indexPage.open();
        indexPage.clickEnterButton();

        var registerPage = new UsersBugredLoginRegistrationPage(driver);
        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        registerPage.sendKeysToNameTextField(name);
        registerPage.sendKeysToEmailTextField(email);
        registerPage.sendKeysToPasswordTextField(password);
        registerPage.clickRegisterButton();

        indexPage = new UsersBugredIndexPage(driver);
        var actualUserName = indexPage.getUserLabelText();

        assertThat(actualUserName)
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }

    @Test
    void testRegisterNegative() {
        var indexPage = new UsersBugredIndexPage(driver);
        indexPage.open();
        indexPage.clickEnterButton();

        var registerPage = new UsersBugredLoginRegistrationPage(driver);
        final var name = FAKER.name().firstName();
        final var email = FAKER.name().lastName();
        final var password = FAKER.internet().password();

        registerPage.sendKeysToNameTextField(name);
        registerPage.sendKeysToEmailTextField(email);
        registerPage.sendKeysToPasswordTextField(password);
        registerPage.clickRegisterButton();

        var actualErrorMessageText = registerPage.getErrorMessageText();

        assertThat(actualErrorMessageText)
            .as("Проверка текста ошибки при неправильном email")
            .isEqualToIgnoringCase(EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE);
    }
}
