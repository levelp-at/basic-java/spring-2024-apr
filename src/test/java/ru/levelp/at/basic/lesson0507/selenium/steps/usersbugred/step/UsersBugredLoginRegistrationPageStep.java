package ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.step;

import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.UsersBugredLoginRegistrationPage;

public class UsersBugredLoginRegistrationPageStep {

    private final WebDriver driver;

    public UsersBugredLoginRegistrationPageStep(WebDriver driver) {
        this.driver = driver;
    }

    public void openPage() {
        new UsersBugredLoginRegistrationPage(driver).open();
    }

    public void registerUser(String name, String email, String password) {
        var registerPage = new UsersBugredLoginRegistrationPage(driver);
        registerPage.sendKeysToNameTextField(name);
        registerPage.sendKeysToEmailTextField(email);
        registerPage.sendKeysToPasswordTextField(password);
        registerPage.clickRegisterButton();
    }

    public String getErrorMessageText() {
        return new UsersBugredLoginRegistrationPage(driver).getErrorMessageText();
    }
}
