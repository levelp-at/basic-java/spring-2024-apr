package ru.levelp.at.basic.lesson0507.selenium.steps.wildberries.step;

import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson0507.selenium.page.components.WildberriesIndexPage;
import ru.levelp.at.basic.utils.SleepUtil;

public class WildberriesIndexPageStep {

    private final WebDriver driver;

    public WildberriesIndexPageStep(WebDriver driver) {
        this.driver = driver;
    }

    public void openPage() {
        new WildberriesIndexPage(driver).open();
    }

    public void selectProductCategory(String categoryName, String subcategoryName, String subSubcategoryName) {
        var indexPage = new WildberriesIndexPage(driver);

        indexPage.header().openMenu();

        SleepUtil.sleep(5000);

        indexPage.header().menu().hoverMenuItem(categoryName);

        SleepUtil.sleep(3500);

        indexPage.header().menu().clickSubmenuItem(subcategoryName);

        SleepUtil.sleep(3500);

        indexPage.header().menu().clickSubSubmenuItem(subSubcategoryName);
    }
}
