package ru.levelp.at.basic.lesson0507.selenium.basic;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SampleSeleniumWindowHandleTest {

    private static final String YA_RU_URL = "https://www.ya.ru/";
    private static final String MAIL_RU_URL = "https://mail.ru";
    private static final String GOOGLE_URL = "https://www.google.com/";

    @Test
    void windowHandleTest() {
        WebDriver driver = new ChromeDriver();

        SleepUtil.sleep(1500);
        System.out.printf("Open URL: %s%n", YA_RU_URL);

        driver.get(YA_RU_URL);
        System.out.println(driver.getWindowHandles());

        SleepUtil.sleep(2000);
        System.out.printf("Open URL: %s%n", MAIL_RU_URL);
        driver = driver.switchTo().newWindow(WindowType.WINDOW);
        driver.get(MAIL_RU_URL);
        System.out.println(driver.getWindowHandles());

        SleepUtil.sleep(2000);
        var handles = driver.getWindowHandles();

        for (String handle : handles) {
            driver = driver.switchTo().window(handle);

            if (driver.getCurrentUrl().startsWith(YA_RU_URL)) {
                break;
            }
        }

        SleepUtil.sleep(2000);
        System.out.printf("Open URL: %s%n", GOOGLE_URL);
        driver = driver.switchTo().newWindow(WindowType.TAB);
        driver.get(GOOGLE_URL);
        System.out.println(driver.getWindowHandles());

        SleepUtil.sleep(2000);
        System.out.printf("Close URL: %s%n", GOOGLE_URL);
        driver.close();
        System.out.println(driver.getWindowHandles());

        SleepUtil.sleep(1500);

        driver.quit();
    }
}
