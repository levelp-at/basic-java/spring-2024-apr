package ru.levelp.at.basic.lesson0507.selenium.basic;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SampleSeleniumFrameTest {

    private static final String YA_RU_URL = "https://www.ya.ru/";
    private static final String MAIL_RU_URL = "https://mail.ru";
    private static final String GOOGLE_URL = "https://www.google.com/";

    @Test
    void frameTest() {
        WebDriver driver = new ChromeDriver();

        SleepUtil.sleep(1500);
        driver.get(MAIL_RU_URL);

        SleepUtil.sleep(1500);
        var loginButton = driver.findElement(By.xpath("//button[text() = 'Войти']"));
        loginButton.click();

        SleepUtil.sleep(1500);
        var frame = driver.findElement(By.xpath("//iframe[contains(@src, 'account.mail.ru/login')]"));
        driver = driver.switchTo().frame(frame);

        driver.findElement(By.xpath("//*[@name = 'username']")).sendKeys("hhjjsjdnjsd");

        SleepUtil.sleep(1500);

        // driver = driver.switchTo().defaultContent();

        driver.quit();
    }
}
