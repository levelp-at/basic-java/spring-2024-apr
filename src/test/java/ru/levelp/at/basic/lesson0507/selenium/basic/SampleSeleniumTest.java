package ru.levelp.at.basic.lesson0507.selenium.basic;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import ru.levelp.at.basic.utils.SleepUtil;

class SampleSeleniumTest {

    @Test
    void openChromeGoogleTest() {
        WebDriver driver = new ChromeDriver();

        SleepUtil.sleep(1500);
        driver.get("https://www.google.com");

        SleepUtil.sleep(1500);
        assertThat(driver.getTitle())
            .as("Проверка названия страницы")
            .isEqualTo("Google");

        driver.quit();
    }

    @Test
    void openMsEdgeGoogleTest() {
        WebDriver driver = new EdgeDriver();

        SleepUtil.sleep(1500);
        driver.get("https://www.google.com");

        SleepUtil.sleep(1500);
        assertThat(driver.getTitle())
            .as("Проверка названия страницы")
            .isEqualTo("Google");

        driver.quit();
    }
}
