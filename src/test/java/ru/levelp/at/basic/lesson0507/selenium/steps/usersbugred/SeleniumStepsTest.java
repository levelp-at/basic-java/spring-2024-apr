package ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.step.UsersBugredIndexPageStep;
import ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred.step.UsersBugredLoginRegistrationPageStep;

class SeleniumStepsTest {

    private static final Faker FAKER = new Faker();
    private static final String EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE
        = "register_not_correct_field (email)";

    private WebDriver driver;
    private UsersBugredIndexPageStep indexPageStep;
    private UsersBugredLoginRegistrationPageStep registrationPageStep;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
        indexPageStep = new UsersBugredIndexPageStep(driver);
        registrationPageStep = new UsersBugredLoginRegistrationPageStep(driver);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void testRegister() {
        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        registrationPageStep.openPage();
        registrationPageStep.registerUser(name, email, password);

        assertThat(indexPageStep.getUserLabelText())
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }

    @Test
    void testRegisterNegative() {
        final var name = FAKER.name().firstName();
        final var email = FAKER.name().lastName();
        final var password = FAKER.internet().password();

        registrationPageStep.openPage();
        registrationPageStep.registerUser(name, email, password);

        assertThat(registrationPageStep.getErrorMessageText())
            .as("Проверка текста ошибки при неправильном email")
            .isEqualToIgnoringCase(EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE);
    }
}
