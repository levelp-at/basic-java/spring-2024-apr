package ru.levelp.at.basic.lesson0507.selenium.waits;

import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class SeleniumImplicitWaitTest {

    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void implicitWaitTest() {
        final var startTime = System.currentTimeMillis();
        try {
            driver.manage().timeouts().implicitlyWait(Duration.ofMillis(4000));
            driver.navigate().to(GOOGLE_URL);

            var element = driver.findElement(By.xpath("//*[@name = 'q']"));
            element.sendKeys("selenium");

            var elementButton = driver.findElement(By.xpath("//*[@name = 'btnK']"));
            elementButton.click();

            var elementLink = driver.findElement(By.xpath("//a/h3[1]"));
            System.out.println(elementLink.getText());
            elementLink.click();
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);
        }
    }
}
