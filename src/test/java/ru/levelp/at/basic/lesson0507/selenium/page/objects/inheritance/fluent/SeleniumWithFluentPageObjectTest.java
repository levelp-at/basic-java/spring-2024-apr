package ru.levelp.at.basic.lesson0507.selenium.page.objects.inheritance.fluent;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class SeleniumWithFluentPageObjectTest {

    private static final Faker FAKER = new Faker();
    private static final String EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE
        = "register_not_correct_field (email)";

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void testRegister() {
        var registerPage = new UsersBugredIndexPage(driver)
            .open()
            .clickEnterButton();

        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        var indexPage = registerPage.sendKeysToNameTextField(name)
                                    .sendKeysToEmailTextField(email)
                                    .sendKeysToPasswordTextField(password)
                                    .clickSuccessRegisterButton();

        var actualUserName = indexPage.getUserLabelText();

        assertThat(actualUserName)
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }

    @Test
    void testRegisterNegative() {
        var registerPage = new UsersBugredIndexPage(driver)
            .open()
            .clickEnterButton();

        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        var actualErrorMessageText = registerPage.sendKeysToNameTextField(name)
                                                 .sendKeysToEmailTextField(email)
                                                 .sendKeysToPasswordTextField(password)
                                                 .clickFailRegisterButton()
                                                 .getErrorMessageText();

        assertThat(actualErrorMessageText)
            .as("Проверка текста ошибки при неправильном email")
            .isEqualToIgnoringCase(EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE);
    }

    @Test
    void testRegisterWithoutTmpVars() {
        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        var actualUserName = new UsersBugredIndexPage(driver)
            .open()
            .clickEnterButton()
            .sendKeysToNameTextField(name)
            .sendKeysToEmailTextField(email)
            .sendKeysToPasswordTextField(password)
            .clickSuccessRegisterButton()
            .getUserLabelText();

        assertThat(actualUserName)
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }
}
