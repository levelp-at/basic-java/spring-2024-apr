package ru.levelp.at.basic.lesson0507.selenium.waits;

import java.time.Duration;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

class SeleniumImplicitVsExplicitWaitTest {

    private static final String GOOGLE_URL = "https://www.google.com";

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void implicitWaitLessThanExplicitWaitTest() {
        final var startTime = System.currentTimeMillis();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(2500));
        wait = new WebDriverWait(driver, Duration.ofMillis(4000));
        driver.navigate().to(GOOGLE_URL);

        var element = driver.findElement(By.xpath("//*[@name = 'q']"));
        element.sendKeys("selenium");

        try {
            var elementButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name = 'btnK1']")));
            elementButton.click();
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);
        }
        //            var elementLink = driver.findElement(By.xpath("//a/h3[1]"));
        //            System.out.println(elementLink.getText());
        //            elementLink.click();

    }

    @Test
    void implicitWaitMoreThanExplicitWaitTest() {
        final var startTime = System.currentTimeMillis();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6500));
        wait = new WebDriverWait(driver, Duration.ofMillis(4000));
        driver.navigate().to(GOOGLE_URL);

        var element = driver.findElement(By.xpath("//*[@name = 'q']"));
        element.sendKeys("selenium");

        try {
            var elementButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name = 'btnK1']")));
            elementButton.click();
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);
        }
        //            var elementLink = driver.findElement(By.xpath("//a/h3[1]"));
        //            System.out.println(elementLink.getText());
        //            elementLink.click();
    }

    @Test
    void implicitWaitTogetherExplicitWaitTest() {
        final var startTime = System.currentTimeMillis();
        driver.manage().timeouts().implicitlyWait(Duration.ofMillis(6500));
        wait = new WebDriverWait(driver, Duration.ofMillis(4000));
        driver.navigate().to(GOOGLE_URL);

        var element = driver.findElement(By.xpath("//*[@name = 'q']"));
        element.sendKeys("selenium");

        // 1. Запоминаем неявное ожидание в переменной
        final var implicitWaitTimeout = driver.manage().timeouts().getImplicitWaitTimeout();

        // 2. Зануляем значение неявного ожидания
        driver.manage().timeouts().implicitlyWait(Duration.ZERO);
        try {
            // 3. Применяем явное ожидание
            var elementButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@name = 'btnK1']")));
            elementButton.click();
        } finally {
            final var endTime = System.currentTimeMillis();
            System.out.printf("Execution time: %d ms%n", endTime - startTime);

            // 4. возвращаем неявное ожидание на место
            driver.manage().timeouts().implicitlyWait(implicitWaitTimeout);
        }
        //            var elementLink = driver.findElement(By.xpath("//a/h3[1]"));
        //            System.out.println(elementLink.getText());
        //            elementLink.click();
    }
}
