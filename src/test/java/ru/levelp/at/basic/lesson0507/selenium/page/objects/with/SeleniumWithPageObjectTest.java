package ru.levelp.at.basic.lesson0507.selenium.page.objects.with;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import ru.levelp.at.basic.lesson0507.selenium.page.objects.sample.UsersBugredIndexPage;
import ru.levelp.at.basic.lesson0507.selenium.page.objects.sample.UsersBugredLoginRegistrationPage;

class SeleniumWithPageObjectTest {

    private static final Faker FAKER = new Faker();

    private WebDriver driver;

    @BeforeEach
    void setUp() {
        driver = new ChromeDriver();
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void testRegister() {
        var indexPage = new UsersBugredIndexPage(driver);
        indexPage.open();
        indexPage.clickEnterButton();

        var registerPage = new UsersBugredLoginRegistrationPage(driver);
        final var name = FAKER.name().firstName();
        final var email = FAKER.internet().emailAddress();
        final var password = FAKER.internet().password();

        registerPage.sendKeysToNameTextField(name);
        registerPage.sendKeysToEmailTextField(email);
        registerPage.sendKeysToPasswordTextField(password);
        registerPage.clickRegisterButton();

        indexPage = new UsersBugredIndexPage(driver);
        var actualUserName = indexPage.getUserLabelText();

        assertThat(actualUserName)
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name);
    }
}
