package ru.levelp.at.basic.lesson0809.api.sample;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

class SampleRestAssuredTest {

    @Test
    void testGetMessengersEndpoint() {
        when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .statusCode(200);
    }

    @Test
    void testGetMessengersEndpointWithResponseBodyValueVerification() {
        when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .statusCode(200)
            .body("data.id", containsInAnyOrder("TELEGRAM", "VIBER", "WHATS_UP"))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testPutMessengersEndpoint() {
        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .pathParam("messengerId", messengerName)
            .when()
            .put("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .statusCode(204);

        when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .statusCode(200)
            .body("data.id", hasItem(messengerName))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testDeleteMessengersEndpoint() {
        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .pathParam("messengerId", messengerName)
            .when()
            .put("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .statusCode(204);

        given()
            .pathParam("messengerId", messengerName)
            .when()
            .delete("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .statusCode(204);

        when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .statusCode(200)
            .body("data.id", not(hasItem(messengerName)))
            .body("meta.pagination.limit", equalTo(10));
    }
}
