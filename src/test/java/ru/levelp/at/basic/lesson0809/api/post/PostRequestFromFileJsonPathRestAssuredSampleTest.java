package ru.levelp.at.basic.lesson0809.api.post;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import com.jayway.jsonpath.JsonPath;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.io.File;
import java.io.IOException;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PostRequestFromFileJsonPathRestAssuredSampleTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testPostMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var email = new Faker().internet().emailAddress();
        final var phoneNumber = new Faker().phoneNumber().cellPhone();
        final var firstName = new Faker().name().firstName();
        final var lastName = new Faker().name().lastName();

        String requestBody = null;
        try {
            requestBody = JsonPath.parse(new File("src/test/resources/ru/levelp/at/basic/lesson0809"
                                      + "/api/post/jsonpath/create_person_request_body.json"))
                                  .set("$.email", email)
                                  .set("$.phoneNumber", phoneNumber)
                                  .set("$.identity.firstName", firstName)
                                  .set("$.identity.lastName", lastName)
                                  .jsonString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .body("data.id", not(empty()))
            .body("data.email", equalTo(email))
            .body("data.phoneNumber", equalTo(phoneNumber))
            .body("data.identity.firstName", equalTo(firstName))
            .body("data.identity.lastName", equalTo(lastName));
    }

    @Test
    void testPostMessengersEndpointWithoutEmail() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var phoneNumber = new Faker().phoneNumber().cellPhone();
        final var firstName = new Faker().name().firstName();
        final var lastName = new Faker().name().lastName();

        String requestBody = null;
        try {
            requestBody = JsonPath.parse(new File("src/test/resources/ru/levelp/at/basic/lesson0809"
                                      + "/api/post/jsonpath/create_person_request_body.json"))
                                  .delete("$.email")
                                  .set("$.phoneNumber", phoneNumber)
                                  .set("$.identity.firstName", firstName)
                                  .set("$.identity.lastName", lastName)
                                  .jsonString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(badRequestResponseSpecification());
    }

    private ResponseSpecification createdResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_CREATED)
            .build();
    }

    private ResponseSpecification badRequestResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_BAD_REQUEST)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
