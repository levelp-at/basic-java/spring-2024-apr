package ru.levelp.at.basic.lesson0809.api.sample;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

class LoggingRequestResponseRestAssuredTest {

    @Test
    void testGetMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetMessengersEndpoint");

        given()
            .log().all(true)
            .when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .log().all(true)
            .statusCode(200);
    }

    @Test
    void testGetMessengersEndpointWithResponseBodyValueVerification() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(),
            "testGetMessengersEndpointWithResponseBodyValueVerification");

        given()
            .log().all(true)
            .when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .log().all(true)
            .statusCode(200)
            .body("data.id", hasItems("TELEGRAM", "VIBER", "WHATS_UP"))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testPutMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPutMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .log().all(true)
            .pathParam("messengerId", messengerName)
            .when()
            .put("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .log().all(true)
            .statusCode(204);

        given()
            .log().all(true)
            .when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .log().all(true)
            .statusCode(200)
            .body("data.id", hasItem(messengerName))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testDeleteMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testDeleteMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .log().all(true)
            .pathParam("messengerId", messengerName)
            .when()
            .put("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .log().all(true)
            .statusCode(204);

        given()
            .log().all(true)
            .pathParam("messengerId", messengerName)
            .when()
            .delete("http://localhost:8080/srv-person-profile/messengers/{messengerId}")
            .then()
            .log().all(true)
            .statusCode(204);

        given()
            .log().all(true)
            .when()
            .get("http://localhost:8080/srv-person-profile/messengers")
            .then()
            .log().all(true)
            .statusCode(200)
            .body("data.id", not(hasItem(messengerName)))
            .body("meta.pagination.limit", equalTo(10));
    }
}
