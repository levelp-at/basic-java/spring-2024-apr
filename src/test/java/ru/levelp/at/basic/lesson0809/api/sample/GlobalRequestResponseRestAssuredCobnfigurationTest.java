package ru.levelp.at.basic.lesson0809.api.sample;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GlobalRequestResponseRestAssuredCobnfigurationTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;
    private ResponseSpecification responseSpecification;

    @BeforeAll
    static void beforeAll() {
        RestAssured.baseURI = PEOPLE_SERVICE_BASE_URL;
        RestAssured.port = PEOPLE_SERVICE_PORT;
        RestAssured.basePath = PEOPLE_SERVICE_BASE_PATH;
    }

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .log(LogDetail.ALL)
            .build();

        responseSpecification = new ResponseSpecBuilder()
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testGetMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetMessengersEndpoint");

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(responseSpecification)
            .statusCode(200);
    }

    @Test
    void testGetMessengersEndpointWithResponseBodyValueVerification() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(),
            "testGetMessengersEndpointWithResponseBodyValueVerification");

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(responseSpecification)
            .statusCode(200)
            .body("data.id", hasItems("TELEGRAM", "VIBER", "WHATS_UP"))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testPutMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPutMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .put("/messengers/{messengerId}")
            .then()
            .spec(responseSpecification)
            .statusCode(204);

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(responseSpecification)
            .statusCode(200)
            .body("data.id", hasItem(messengerName))
            .body("meta.pagination.limit", equalTo(10));
    }

    @Test
    void testDeleteMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testDeleteMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .put("/messengers/{messengerId}")
            .then()
            .spec(responseSpecification)
            .statusCode(204);

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .delete("/messengers/{messengerId}")
            .then()
            .spec(responseSpecification)
            .statusCode(204);

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .log().all(true)
            .spec(responseSpecification)
            .body("data.id", not(hasItem(messengerName)))
            .body("meta.pagination.limit", equalTo(10));
    }
}
