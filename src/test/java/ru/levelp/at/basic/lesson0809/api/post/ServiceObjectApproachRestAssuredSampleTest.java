package ru.levelp.at.basic.lesson0809.api.post;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.util.Map;
import java.util.stream.Stream;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.levelp.at.basic.lesson0809.api.client.PeopleApiClient;
import ru.levelp.at.basic.lesson0809.api.model.CreatePersonRequest;
import ru.levelp.at.basic.lesson0809.api.model.PersonListResponse;
import ru.levelp.at.basic.lesson0809.api.model.PersonResponse;

class ServiceObjectApproachRestAssuredSampleTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";
    private RequestSpecification requestSpecification;
    private PeopleApiClient peopleApiClient;

    static Stream<Arguments> peopleGetQueryParamDataProvider() {
        return Stream.of(
            Arguments.of(Map.of("limit", 1)),
            Arguments.of(Map.of("limit", 3))
        );
    }

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();

        peopleApiClient = new PeopleApiClient(requestSpecification);
    }

    @Test
    void testPostPeopleEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostPeopleEndpoint");

        final var requestBody = CreatePersonRequest.defaultRequest().build();

        var response = peopleApiClient.createPerson(requestBody)
                                      .then()
                                      .spec(createdResponseSpecification())
                                      .extract().as(PersonResponse.class);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.data().id())
                  .as("Провека что id не null").isNotEmpty();
            softly.assertThat(response.data())
                  .as("Проверка тела ответа")
                  .usingRecursiveComparison()
                  .ignoringFields("id")
                  .isEqualTo(requestBody);
        });
    }

    @Test
    void testGetPeopleEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetPeopleEndpoint");

        var response = peopleApiClient.getPeople()
                                      .then()
                                      .spec(okResponseSpecification())
                                      .extract().as(PersonListResponse.class);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.data())
                  .as("Проверка количества записей")
                  .hasSize(10);
        });
    }

    @ParameterizedTest
    @MethodSource("peopleGetQueryParamDataProvider")
    void testGetPeopleQueryParamEndpoint(Map<String, Object> queryParam) {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetPeopleQueryParamEndpoint");

        var response = peopleApiClient.getPeople(queryParam)
                                      .then()
                                      .spec(okResponseSpecification())
                                      .extract().as(PersonListResponse.class);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.data())
                  .as("Проверка количества записей")
                  .hasSize((Integer) queryParam.get("limit"));
        });
    }

    private ResponseSpecification okResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_OK)
            .build();
    }

    private ResponseSpecification createdResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_CREATED)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
