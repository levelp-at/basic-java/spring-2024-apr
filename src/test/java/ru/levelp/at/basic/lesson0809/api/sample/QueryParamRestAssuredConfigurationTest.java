package ru.levelp.at.basic.lesson0809.api.sample;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.util.Map;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class QueryParamRestAssuredConfigurationTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testGetMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetMessengersEndpoint");

        given()
            .queryParams(Map.of("limit", 15,
                "offset", 3))
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(okResponseSpecification())
            .body("meta.pagination.limit", equalTo(15))
            .body("meta.pagination.offset", equalTo(3));
    }

    private ResponseSpecification okResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_OK)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
