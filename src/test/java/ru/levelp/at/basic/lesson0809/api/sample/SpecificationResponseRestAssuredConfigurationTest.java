package ru.levelp.at.basic.lesson0809.api.sample;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class SpecificationResponseRestAssuredConfigurationTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testGetMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testGetMessengersEndpoint");

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(okResponseSpecification());
    }

    @Test
    void testGetMessengersEndpointWithResponseBodyValueVerification() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(),
            "testGetMessengersEndpointWithResponseBodyValueVerification");

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(okResponseSpecification())
            .body("data.id", hasItems("TELEGRAM", "VIBER", "WHATS_UP"));
    }

    @Test
    void testPutMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPutMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .put("/messengers/{messengerId}")
            .then()
            .spec(noContentResponseSpecification());

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .spec(okResponseSpecification())
            .body("data.id", hasItem(messengerName));
    }

    @Test
    void testDeleteMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testDeleteMessengersEndpoint");

        final var messengerName = new Faker().app().name().toUpperCase();

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .put("/messengers/{messengerId}")
            .then()
            .spec(noContentResponseSpecification());

        given()
            .spec(requestSpecification)
            .pathParam("messengerId", messengerName)
            .when()
            .delete("/messengers/{messengerId}")
            .then()
            .spec(noContentResponseSpecification());

        given()
            .spec(requestSpecification)
            .when()
            .get("/messengers")
            .then()
            .log().all(true)
            .spec(okResponseSpecification())
            .body("data.id", not(hasItem(messengerName)));
    }

    private ResponseSpecification noContentResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_NO_CONTENT)
            .build();
    }

    private ResponseSpecification okResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_OK)
            .expectBody("meta.pagination.limit", equalTo(10))
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
