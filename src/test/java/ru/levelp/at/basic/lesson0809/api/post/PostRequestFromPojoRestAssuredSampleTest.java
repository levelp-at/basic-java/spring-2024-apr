package ru.levelp.at.basic.lesson0809.api.post;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.time.LocalDate;
import java.time.ZoneId;
import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.levelp.at.basic.lesson0809.api.model.AddressData;
import ru.levelp.at.basic.lesson0809.api.model.CreatePersonRequest;
import ru.levelp.at.basic.lesson0809.api.model.CreatePersonRequestVanillaJava;
import ru.levelp.at.basic.lesson0809.api.model.IdentityData;
import ru.levelp.at.basic.lesson0809.api.model.PassportData;
import ru.levelp.at.basic.lesson0809.api.model.PersonResponse;

class PostRequestFromPojoRestAssuredSampleTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testPostMessengersWithRequestBodySerializationEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var faker = new Faker();

        final var requestBody = new CreatePersonRequest("WORK_INSPECTOR",
            faker.internet().emailAddress(), faker.phoneNumber().cellPhone(), faker.company().name(),
            new IdentityData(faker.name().firstName(), faker.name().lastName(),
                faker.name().username(), "MALE",
                faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.address().cityName(), new PassportData("1234", "444444",
                "", LocalDate.parse("1980-02-07"), "123-456")),
            new AddressData("", 1, 2, "", 1, "", ""));

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .body("data.id", not(empty()))
            .body("data.email", equalTo(requestBody.email()))
            .body("data.identity.firstName", equalTo(requestBody.identity().firstName()))
            .body("data.identity.dateOfBirth", equalTo(requestBody.identity().dateOfBirth().toString()));
    }

    @Test
    void testPostMessengersWithResponseBodyDeserializationEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var faker = new Faker();

        final var requestBody = new CreatePersonRequest("WORK_INSPECTOR",
            faker.internet().emailAddress(), faker.phoneNumber().cellPhone(), faker.company().name(),
            new IdentityData(faker.name().firstName(), faker.name().lastName(),
                faker.name().username(), "MALE",
                faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.address().cityName(), new PassportData("1234", "444444",
                "", LocalDate.parse("1980-02-07"), "123-456")),
            new AddressData("", 1, 2, "", 1, "", ""));

        PersonResponse response = given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .extract().as(PersonResponse.class);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.data().id())
                  .as("Провека что id не null").isNotEmpty();
            softly.assertThat(response.data())
                  .as("Проверка тела ответа")
                  .usingRecursiveComparison()
                  .ignoringFields("id")
                  .isEqualTo(requestBody);
        });
    }

    @Test
    void testPostMessengersWithNullEmailEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var faker = new Faker();

        final var requestBody = new CreatePersonRequest("WORK_INSPECTOR",
            null, faker.phoneNumber().cellPhone(), faker.company().name(),
            new IdentityData(faker.name().firstName(), faker.name().lastName(),
                faker.name().username(), "MALE",
                faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                faker.address().cityName(), new PassportData("1234", "444444",
                "", LocalDate.parse("1980-02-07"), "123-456")),
            new AddressData("", 1, 2, "", 1, "", ""));

        PersonResponse response = given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .extract().as(PersonResponse.class);

        SoftAssertions.assertSoftly(softly -> {
            softly.assertThat(response.data().id())
                  .as("Провека что id не null").isNotEmpty();
            softly.assertThat(response.data())
                  .as("Проверка тела ответа")
                  .usingRecursiveComparison()
                  .ignoringFields("id")
                  .isEqualTo(requestBody);
        });
    }

    @Test
    void testPostMessengersWithNullEmailAndBuilderPatternEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var faker = new Faker();

        final var requestBody = CreatePersonRequest
            .builder()
            .role("WORK_INSPECTOR")
            .email(null)
            .phoneNumber(faker.phoneNumber().cellPhone())
            .placeOfWork(faker.company().name())
            .identity(IdentityData
                .builder()
                .firstName(faker.name().firstName())
                .lastName(faker.name().lastName())
                .middleName(faker.name().username())
                .gender("MALE")
                .dateOfBirth(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .placeOfBirth(faker.address().cityName())
                .passport(PassportData
                    .builder()
                    .series("1234")
                    .number("444444")
                    .placeOfIssue("")
                    .dateOfIssue(LocalDate.parse("1980-02-07"))
                    .departmentCode("123-456")
                    .build())
                .build())
            .addressData(AddressData
                .builder()
                .street("")
                .city("")
                .flat(12)
                .houseBuilding(33)
                .houseLetter("F")
                .houseNumber(32)
                .postalCode("")
                .build())
            .build();

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(badRequestResponseSpecification());
    }

    @Test
    void testPostMessengersWithNullEmailAndFactoryMethodPatternEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var requestBody = CreatePersonRequest
            .defaultRequest()
            .email(null)
            .identity(IdentityData.defaultIdentity()
                                  .passport(PassportData.defaultPassport()
                                                        .series(null)
                                                        .build())
                                  .build())
            .build();

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(badRequestResponseSpecification());
    }

    @Test
    void testPostMessengersVanillaJavaBuilderWithDefaultValuesEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var requestBody = CreatePersonRequestVanillaJava
            .builder()
            .email(null)
            .identity(IdentityData.defaultIdentity()
                                  .passport(PassportData.defaultPassport()
                                                        .series(null)
                                                        .build())
                                  .build())
            .build();

        given()
            .spec(requestSpecification)
            .body(requestBody)
            .when()
            .post("/people")
            .then()
            .spec(badRequestResponseSpecification());
    }

    private ResponseSpecification createdResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_CREATED)
            .build();
    }

    private ResponseSpecification badRequestResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_BAD_REQUEST)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
