package ru.levelp.at.basic.lesson0809.api.post;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PostRequestRestAssuredSampleTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testPostMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var email = new Faker().internet().emailAddress();

        var requestBody = """
            {
                          "role": "LECTOR",
                          "email": "%s",
                          "phoneNumber": "+79211234567",
                          "placeOfWork": "Engineer",
                          "identity": {
                            "firstName": "Vasily",
                            "lastName": "Pupkin",
                            "middleName": "Ivanovich",
                            "gender": "MALE",
                            "dateOfBirth": "1980-02-07",
                            "placeOfBirth": "Moscow",
                            "passport": {
                              "series": "1234",
                              "number": "123456",
                              "placeOfIssue": "",
                              "dateOfIssue": "1980-02-07",
                              "departmentCode": "123-456"
                            }
                          },
                          "address": {
                            "street": "Beethovenstrasse",
                            "houseNumber": 12,
                            "houseBuilding": 1,
                            "houseLetter": "A",
                            "flat": 123,
                            "city": "Moscow",
                            "postalCode": "123456"
                          }
                        }""";

        given()
            .spec(requestSpecification)
            .body(requestBody.formatted(email))
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .body("data.id", not(empty()))
            .body("data.email", equalTo(email));
    }

    private ResponseSpecification createdResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_CREATED)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
