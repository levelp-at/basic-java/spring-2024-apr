package ru.levelp.at.basic.lesson0809.api.post;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

import com.github.javafaker.Faker;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.apache.http.HttpStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PostRequestFromFilePlaceholdersRestAssuredSampleTest {

    private static final String PEOPLE_SERVICE_BASE_URL = "http://localhost";
    private static final int PEOPLE_SERVICE_PORT = 8080;
    private static final String PEOPLE_SERVICE_BASE_PATH = "/srv-person-profile";

    private RequestSpecification requestSpecification;

    @BeforeEach
    void setUp() {
        requestSpecification = new RequestSpecBuilder()
            .setBaseUri(PEOPLE_SERVICE_BASE_URL)
            .setPort(PEOPLE_SERVICE_PORT)
            .setBasePath(PEOPLE_SERVICE_BASE_PATH)
            .setContentType(ContentType.JSON)
            .setAccept(ContentType.JSON)
            .log(LogDetail.ALL)
            .build();
    }

    @Test
    void testPostMessengersEndpoint() {
        System.out.printf("Test %s.%s%n", this.getClass().getSimpleName(), "testPostMessengersEndpoint");

        final var email = new Faker().internet().emailAddress();
        final var phoneNumber = new Faker().phoneNumber().cellPhone();
        final var firstName = new Faker().name().firstName();

        String requestBody = "";
        try {
            requestBody = Files.readString(Paths.get("src/test/resources/ru/levelp/at/basic/lesson0809"
                + "/api/post/placeholders/create_person_request_body.json"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        given()
            .spec(requestSpecification)
            .body(requestBody
                .replace("{email}", email)
                .replace("{phoneNumber}", phoneNumber)
                .replace("{firstName}", firstName)
            )
            .when()
            .post("/people")
            .then()
            .spec(createdResponseSpecification())
            .body("data.id", not(empty()))
            .body("data.email", equalTo(email));
    }

    private ResponseSpecification createdResponseSpecification() {
        return responseSpecBuilder()
            .expectStatusCode(HttpStatus.SC_CREATED)
            .build();
    }

    private ResponseSpecBuilder responseSpecBuilder() {
        return new ResponseSpecBuilder()
            .log(LogDetail.ALL);
    }
}
