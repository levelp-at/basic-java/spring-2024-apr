package ru.levelp.at.basic.lesson12.design.patterns.builder;

import com.github.javafaker.Faker;
import java.time.ZoneId;
import org.junit.jupiter.api.Test;

class PersonBuilderTest {

    @Test
    void builderTest() {
        final var faker = new Faker();
        var person = PersonWithBuilder
            .builder()
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName())
            .middleName(faker.name().username())
            .birthDate(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .build();

        System.out.println(person);
    }

    @Test
    void builderWithoutFirstNameTest() {
        final var faker = new Faker();
        var person = PersonWithBuilder
            .builder()
            .lastName(faker.name().lastName())
            .middleName(faker.name().username())
            .birthDate(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .build();

        System.out.println(person);
    }

    @Test
    void builderWithoutBirthDateTest() {
        final var faker = new Faker();
        var person = PersonWithBuilder
            .builder()
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName())
            .middleName(faker.name().username())
            .build();

        System.out.println(person);
    }

    @Test
    void builderWithoutTwoParamsTest() {
        final var faker = new Faker();
        var person = PersonWithBuilder
            .builder()
            .firstName(faker.name().firstName())
            .birthDate(faker.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .build();

        System.out.println(person);
    }
}
