package ru.levelp.at.basic.lesson12.design.patterns.singleton.tags;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class TagName {

    public static final String UI_TAG = "UI";
    public static final String WILDBERRIES_TAG = "wildberries";
    public static final String USERSBUGRED_TAG = "usersbugred";
    public static final String POSITIVE_TAG = "positive";
    public static final String NEGATIVE_TAG = "negative";
}
