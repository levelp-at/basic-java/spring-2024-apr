package ru.levelp.at.basic.lesson12.design.patterns.factory.method.wildberries.step;

import org.openqa.selenium.WebDriver;

public class WildberriesProductPageStep {

    private final WebDriver driver;

    public WildberriesProductPageStep(WebDriver driver) {
        this.driver = driver;
    }
}
