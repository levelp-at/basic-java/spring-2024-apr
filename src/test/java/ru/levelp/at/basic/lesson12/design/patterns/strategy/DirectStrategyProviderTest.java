package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class DirectStrategyProviderTest {

    static Stream<Arguments> rubStrategyDataProvider() {
        return Stream.of(
            Arguments.of(new RubTaxCalculatorImpl(), new BigDecimal("1000000"), new BigDecimal("130000.00")),
            Arguments.of(new RubTaxCalculatorImpl(), new BigDecimal("6000000"), new BigDecimal("900000.00"))
        );
    }

    static Stream<Arguments> tryStrategyDataProvider() {
        return Stream.of(
            Arguments.of(new TryTaxCalculatorImpl(), new BigDecimal("100000"), new BigDecimal("15000.00")),
            Arguments.of(new TryTaxCalculatorImpl(), new BigDecimal("200000"), new BigDecimal("40000.00")),
            Arguments.of(new TryTaxCalculatorImpl(), new BigDecimal("500000"), new BigDecimal("135000.00")),
            Arguments.of(new TryTaxCalculatorImpl(), new BigDecimal("2000000"), new BigDecimal("700000.00")),
            Arguments.of(new TryTaxCalculatorImpl(), new BigDecimal("4000000"), new BigDecimal("1600000.00"))
        );
    }

    @ParameterizedTest
    @MethodSource("rubStrategyDataProvider")
    void rubStrategyTest(TaxCalculator calculator, BigDecimal startAmount, BigDecimal expectedAmount) {
        var provider = new DirectTaxStrategyProviderImpl(calculator);
        var actualAmount = provider.calculate(startAmount);

        assertThat(actualAmount)
            .as("Проверка корректности расчёта налогов в России")
            .isEqualTo(expectedAmount);
    }

    @ParameterizedTest
    @MethodSource("tryStrategyDataProvider")
    void tryStrategyTest(TaxCalculator calculator, BigDecimal startAmount, BigDecimal expectedAmount) {
        var provider = new DirectTaxStrategyProviderImpl(calculator);
        var actualAmount = provider.calculate(startAmount);

        assertThat(actualAmount)
            .as("Проверка корректности расчёта налогов в Турции")
            .isEqualTo(expectedAmount);
    }
}
