package ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred;

import static org.assertj.core.api.Assertions.assertThat;

import com.github.javafaker.Faker;
import io.qameta.allure.Allure;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Tags;
import org.junit.jupiter.api.Test;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.SeleniumBaseTest;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.tags.TagName;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.tags.annotation.NegativeTag;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.tags.annotation.PositiveTag;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred.step.UsersBugredIndexPageStep;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred.step.UsersBugredLoginRegistrationPageStep;

@DisplayName("Тесты на функционал регистрации usersbugred")
@Tags({@Tag(TagName.UI_TAG), @Tag(TagName.USERSBUGRED_TAG)})
@Epic("Usersbugred epic")
@TmsLink("LUP-5")
class SeleniumStepsTest extends SeleniumBaseTest {

    private static final Faker FAKER = new Faker();
    private static final String EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE
        = "register_not_correct_field (email)";
    private static final String PETROV_TESTER = "v.petrov";

    private UsersBugredIndexPageStep indexPageStep;
    private UsersBugredLoginRegistrationPageStep registrationPageStep;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();

        indexPageStep = new UsersBugredIndexPageStep(driver);
        registrationPageStep = new UsersBugredLoginRegistrationPageStep(driver);
    }

    @Test
    @DisplayName("Успешная регистрация пользователя")
    @Owner(PETROV_TESTER)
    @Severity(SeverityLevel.BLOCKER)
    @PositiveTag
    @Feature("Регистрация")
    @Story("Регистрация пользователя")
    @TmsLink("LUP-4")
    void testRegister() {
        Allure.step("Генерация тестовых данных");
        String name = FAKER.name().firstName();
        String email = FAKER.internet().emailAddress();
        String password = FAKER.internet().password();

        registrationPageStep.openPage();
        registrationPageStep.registerUser(name, email, password);

        Allure.step("Проверка, что ", () -> assertThat(indexPageStep.getUserLabelText())
            .as("Проверка имени зарегистрированного пользователя")
            .isEqualToIgnoringCase(name));
    }

    @Test
    @DisplayName("Проверка ошибки при не правильном формате email при регистрации пользователя")
    @Owner("s.sidorov")
    @Severity(SeverityLevel.TRIVIAL)
    @NegativeTag
    @Feature("Регистрация")
    @Story("Обработка неверных данных при регистрации пользователя")
    @TmsLink("LUP-6")
    void testRegisterNegative() {
        Allure.step("Генерация тестовых данных");
        final var name = FAKER.name().firstName();
        final var email = FAKER.name().lastName();
        final var password = FAKER.internet().password();

        registrationPageStep.openPage();
        registrationPageStep.registerUser(name, email, password);

        Allure.step("Проверка сообщения об ошибке", () -> assertThat(registrationPageStep.getErrorMessageText())
            .as("Проверка текста ошибки при неправильном email")
            .isEqualToIgnoringCase(EXPECTED_INVALID_EMAIL_REGISTRATION_ERROR_MESSAGE));
    }

    @Test
    @DisplayName("Проверка ошибки при не правильном формате email при регистрации пользователя падающий")
    @Owner("s.sidorov")
    @Severity(SeverityLevel.BLOCKER)
    @NegativeTag
    @Feature("Регистрация")
    @Story("Обработка неверных данных при регистрации пользователя")
    @Issue("LUP-1")
    @TmsLink("LUP-2")
    void testRegisterNegativeFail() {
        Allure.step("Генерация тестовых данных");
        final var name = FAKER.name().firstName();
        final var email = FAKER.name().lastName();
        final var password = FAKER.internet().password();

        registrationPageStep.openPage();
        registrationPageStep.registerUser(name, email, password);

        Allure.step("Проверка сообщения об ошибке", () -> assertThat(registrationPageStep.getErrorMessageText())
            .as("Проверка текста ошибки при неправильном email")
            .isEmpty());
    }
}
