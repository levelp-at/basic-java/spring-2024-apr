package ru.levelp.at.basic.lesson12.design.patterns.factory.method;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.extension.AllureAttachmentExtension;

@ExtendWith(AllureAttachmentExtension.class)
public abstract class SeleniumBaseTest {

    public WebDriver driver;

    @BeforeEach
    protected void setUp() {
        driver = Allure.step("Открытие браузера", WebDriverSingleton::getDriver);
    }

    @AfterEach
    protected void tearDown() {
        Allure.step("Закрытие браузера", WebDriverSingleton::close);
    }
}
