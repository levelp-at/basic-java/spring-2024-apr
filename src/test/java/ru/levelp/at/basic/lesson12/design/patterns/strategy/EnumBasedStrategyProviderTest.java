package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import ru.levelp.at.basic.lesson12.design.patterns.strategy.dictionary.CurrencyDictionary;

class EnumBasedStrategyProviderTest {

    static Stream<Arguments> rubStrategyDataProvider() {
        return Stream.of(
            Arguments.of(CurrencyDictionary.RUB, new BigDecimal("1000000"), new BigDecimal("130000.00")),
            Arguments.of(CurrencyDictionary.RUB, new BigDecimal("6000000"), new BigDecimal("900000.00"))
        );
    }

    static Stream<Arguments> tryStrategyDataProvider() {
        return Stream.of(
            Arguments.of(CurrencyDictionary.TRY, new BigDecimal("100000"), new BigDecimal("15000.00")),
            Arguments.of(CurrencyDictionary.TRY, new BigDecimal("200000"), new BigDecimal("40000.00")),
            Arguments.of(CurrencyDictionary.TRY, new BigDecimal("500000"), new BigDecimal("135000.00")),
            Arguments.of(CurrencyDictionary.TRY, new BigDecimal("2000000"), new BigDecimal("700000.00")),
            Arguments.of(CurrencyDictionary.TRY, new BigDecimal("4000000"), new BigDecimal("1600000.00"))
        );
    }

    @ParameterizedTest
    @MethodSource("rubStrategyDataProvider")
    void rubStrategyTest(CurrencyDictionary currency, BigDecimal startAmount, BigDecimal expectedAmount) {
        var provider = new EnumBasedTaxStrategyProviderImpl(currency);
        var actualAmount = provider.calculate(startAmount);

        assertThat(actualAmount)
            .as("Проверка корректности расчёта налогов в России")
            .isEqualTo(expectedAmount);
    }

    @ParameterizedTest
    @MethodSource("tryStrategyDataProvider")
    void tryStrategyTest(CurrencyDictionary currency, BigDecimal startAmount, BigDecimal expectedAmount) {
        var provider = new EnumBasedTaxStrategyProviderImpl(currency);
        var actualAmount = provider.calculate(startAmount);

        assertThat(actualAmount)
            .as("Проверка корректности расчёта налогов в Турции")
            .isEqualTo(expectedAmount);
    }
}
