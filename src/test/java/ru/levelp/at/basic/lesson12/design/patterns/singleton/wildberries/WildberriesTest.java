package ru.levelp.at.basic.lesson12.design.patterns.singleton.wildberries;

import static org.assertj.core.api.Assertions.assertThat;

import io.qameta.allure.Allure;
import io.qameta.allure.Epic;
import io.qameta.allure.Feature;
import io.qameta.allure.Owner;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import ru.levelp.at.basic.lesson1011.cicd.wildberries.WildberriesProductPage;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.SeleniumBaseTest;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.tags.TagName;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.wildberries.step.WildberriesIndexPageStep;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.wildberries.step.WildberriesProductPageStep;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.wildberries.step.WildberriesProductsPageStep;
import ru.levelp.at.basic.utils.SleepUtil;

@DisplayName("Страница товара Wildberries")
@Owner("d.khodakovskiy")
@Severity(SeverityLevel.CRITICAL)
@Tag("UI")
@Epic("Wildberries epic")
@Feature("Products page")
class WildberriesTest extends SeleniumBaseTest {

    private WildberriesIndexPageStep indexPageStep;
    private WildberriesProductsPageStep productsPageStep;
    private WildberriesProductPageStep productPageStep;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();

        indexPageStep = new WildberriesIndexPageStep(driver);
        productsPageStep = new WildberriesProductsPageStep(driver);
        productPageStep = new WildberriesProductPageStep(driver);
    }

    @Test
    @DisplayName("Проверка цены товара на его странице после открытия с общей страницы товаров")
    @Tag(TagName.WILDBERRIES_TAG)
    @Story("Страница товара")
    void sampleCompositePageObjectTest() {
        indexPageStep.openPage();
        SleepUtil.sleep(3500);

        indexPageStep.selectProductCategory("Электроника", "Смартфоны и телефоны", "Смартфоны");
        SleepUtil.sleep(3500);

        var card = productsPageStep.selectCard(3);
        SleepUtil.sleep(3500);

        final var cardPriceWithoutAccount = card.getPriceWithoutWbAccount();
        final var cardPriceWithAccount = card.getPriceWithWbAccount();

        System.out.printf("price without account %s%n", cardPriceWithoutAccount);
        System.out.printf("price with account %s%n", cardPriceWithAccount);
        SleepUtil.sleep(3500);

        card.click();

        SleepUtil.sleep(3500);

        var productPage = new WildberriesProductPage(driver);
        final var actualPriceWithoutAccount = productPage.getPriceWithoutWbAccountLabel();
        final var actualPriceWithAccount = productPage.getPriceWithWbAccountLabel();

        Allure.step("Проверка цены", () -> {
            assertThat(actualPriceWithoutAccount)
                .as("Провека цены без аккаунта WB")
                .isEqualTo(cardPriceWithoutAccount);

            assertThat(actualPriceWithAccount)
                .as("Провека цены c аккаунтом WB")
                .isEqualTo(cardPriceWithAccount);
        });
    }
}
