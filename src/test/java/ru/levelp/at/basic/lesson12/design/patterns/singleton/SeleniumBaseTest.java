package ru.levelp.at.basic.lesson12.design.patterns.singleton;

import io.qameta.allure.Allure;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson12.design.patterns.singleton.extension.AllureAttachmentExtension;

@ExtendWith(AllureAttachmentExtension.class)
public abstract class SeleniumBaseTest {

    protected WebDriver driver;

    @BeforeEach
    protected void setUp() {
        //            var options = new ChromeOptions();
        //            var headless = Boolean.parseBoolean(System.getProperty("browser.headless"));
        //
        //            if (headless) {
        //                options.addArguments("--headless=new", "--disable-dev-shm-usage", "--no-sandbox");
        //            }
        // new ChromeDriver(options);

        driver = Allure.step("Открытие браузера", WebDriverSingleton::getDriver);

        // TestContext.getInstance().put("driver", driver);
    }

    @AfterEach
    protected void tearDown() {
        // Allure.step("Закрытие браузера", () -> driver.quit());
        // TestContext.clearContext();
        WebDriverSingleton.close();
    }
}
