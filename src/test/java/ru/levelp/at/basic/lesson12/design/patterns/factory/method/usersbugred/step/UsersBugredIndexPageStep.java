package ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred.step;

import org.openqa.selenium.WebDriver;
import ru.levelp.at.basic.lesson1011.cicd.usersbugred.UsersBugredIndexPage;

public class UsersBugredIndexPageStep {

    private final WebDriver driver;

    public UsersBugredIndexPageStep(WebDriver driver) {
        this.driver = driver;
    }

    public String getUserLabelText() {
        return new UsersBugredIndexPage(driver).getUserLabelText();
    }
}
