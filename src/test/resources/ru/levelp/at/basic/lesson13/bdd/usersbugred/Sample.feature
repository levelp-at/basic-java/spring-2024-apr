Feature: Register user functionality usersbugred

    Scenario: Positive registration user
        Given I open usersbugred web-site
        And I navigate to Login Registration page
        When I enter username "someTestUser" in username text field in Registration block on Login Registration page
        And I enter email "email@sometest.user.com" in email text field in Registration block on Login Registration page
        And I enter password "pass12312" in password text field in Registration block on Login Registration page
        And I click on button "Зарегистрироваться" in Registration block on Login Registration page
        Then username in user dropdown should be equal "someTestUser" on Main page

    Scenario: Positive registration user less steps
        Given I open usersbugred web-site
        And I navigate to Login Registration page
        When I register user in registration form in Registration block on Login Registration page with parameters
#            | username           | email                               | password    |
#            | someTestUser545656 | email12312@sometest1231231.user.com | jdueuiweuwe |

            | username | someTestUser545656                  |
            | email    | email12312@sometest1231231.user.com |
            | password | jdueuiweuwe                         |
        Then username in user dropdown should be equal "someTestUser" on Main page

    Scenario: Positive registration user less steps
        Given I open usersbugred web-site
#        Then usersbugred web-site should be open таких проверок быть не должно по концепции BDD
        And I navigate to Login Registration page
        When I register user in registration form in Registration block on Login Registration page with parameters
#            | username           | email                               | password    |
#            | someTestUser545656 | email12312@sometest1231231.user.com | jdueuiweuwe |

            | username | someTestUser545656                  |
            | email    | email12312@sometest1231231.user.com |
            | password | jdueuiweuwe                         |
        Then username in user dropdown should be equal "someTestUser" on Main page
