package ru.levelp.at.basic.lesson13.bdd.wildberries.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class MenuElement extends AbstractElement {

    private static final By MAIN_MENU_XPATH = By.xpath("//ul[@class = 'menu-burger__main-list']"
        + "/li/a[contains(@class, 'menu-burger__main-list-link')]");
    private static final By SUBMENU_XPATH = By.xpath("//ul[@class = 'menu-burger__set']"
        + "/li/span[contains(@class, 'menu-burger__link')]");
    private static final By SUB_SUBMENU_XPATH = By.xpath("//ul[@class = 'menu-burger__set']"
        + "/li/a[contains(@class, 'menu-burger__link')]");

    public MenuElement(WebDriver driver) {
        super(driver);
    }

    public void hoverMenuItem(final String menuItemName) {
        var menuItems = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(MAIN_MENU_XPATH, 5));

        WebElement menuItem = menuItems.stream()
                                       .filter(element -> element.getText().contains(menuItemName))
                                       .findFirst()
                                       .orElseThrow();

        new Actions(driver).moveToElement(menuItem).perform();
    }

    public void clickSubmenuItem(final String submenuItemName) {
        var menuItems = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(SUBMENU_XPATH, 5));

        WebElement menuItem = menuItems.stream()
                                       .filter(element -> element.getText().contains(submenuItemName))
                                       .findFirst()
                                       .orElseThrow();

        wait.until(ExpectedConditions.elementToBeClickable(menuItem)).click();
    }

    public void clickSubSubmenuItem(final String submenuItemName) {
        var menuItems = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(SUB_SUBMENU_XPATH, 3));

        WebElement menuItem = menuItems.stream()
                                       .filter(element -> element.getText().contains(submenuItemName))
                                       .findFirst()
                                       .orElseThrow();

        wait.until(ExpectedConditions.elementToBeClickable(menuItem)).click();
    }
}
