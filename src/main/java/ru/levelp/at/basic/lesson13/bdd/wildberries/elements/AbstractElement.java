package ru.levelp.at.basic.lesson13.bdd.wildberries.elements;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractElement {

    protected final WebDriver driver;
    protected final WebDriverWait wait;

    protected AbstractElement(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(5000));

        PageFactory.initElements(driver, this);
    }
}
