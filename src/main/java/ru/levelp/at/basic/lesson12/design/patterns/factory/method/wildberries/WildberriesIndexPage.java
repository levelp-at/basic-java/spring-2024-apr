package ru.levelp.at.basic.lesson12.design.patterns.factory.method.wildberries;

import org.openqa.selenium.WebDriver;

public class WildberriesIndexPage extends AbstractWildberriesPage {

    public WildberriesIndexPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open("/");
    }
}
