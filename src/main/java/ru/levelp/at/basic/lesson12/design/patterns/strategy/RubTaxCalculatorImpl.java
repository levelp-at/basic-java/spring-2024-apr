package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import java.math.BigDecimal;

public class RubTaxCalculatorImpl implements TaxCalculator {

    private static final BigDecimal LOW_TAX_RATE = new BigDecimal("0.13");
    private static final BigDecimal HIGH_TAX_RATE = new BigDecimal("0.15");
    private static final BigDecimal YEARLY_INCOME_FOR_HIGH_TAX_RATE = new BigDecimal("5000000");

    @Override
    public BigDecimal calculateTax(BigDecimal amount) {
        if (YEARLY_INCOME_FOR_HIGH_TAX_RATE.compareTo(amount) < 0) {
            return amount.multiply(HIGH_TAX_RATE);
        }

        return amount.multiply(LOW_TAX_RATE);
    }
}
