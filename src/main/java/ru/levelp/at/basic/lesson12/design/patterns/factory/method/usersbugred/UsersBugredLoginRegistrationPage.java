package ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UsersBugredLoginRegistrationPage extends UsersBugredAbstractPage {

    private static final String RELATIVE_URL = "/user/login/index.html";

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='name']")
    private WebElement nameTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='email']")
    private WebElement emailTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='act_register_now']")
    private WebElement registerButton;

    @FindBy(xpath = "//form[contains(@action, '/register/')]/p")
    private WebElement errorMessageLabel;

    public UsersBugredLoginRegistrationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open(RELATIVE_URL);
    }

    public void sendKeysToNameTextField(String name) {
        sendKeysToElement(nameTextField, name);
    }

    public void sendKeysToEmailTextField(String email) {
        sendKeysToElement(emailTextField, email);
    }

    public void sendKeysToPasswordTextField(String password) {
        sendKeysToElement(passwordTextField, password);
    }

    public void clickRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
    }

    public String getErrorMessageText() {
        return wait.until(ExpectedConditions.visibilityOf(errorMessageLabel)).getText();
    }
}
