package ru.levelp.at.basic.lesson12.design.patterns.strategy.dictionary;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter
@ToString
public enum CurrencyDictionary {

    RUB("RUB"),
    TRY("TRY");

    private final String alphaCode;
}
