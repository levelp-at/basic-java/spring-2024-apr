package ru.levelp.at.basic.lesson12.design.patterns.factory.method.usersbugred;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UsersBugredIndexPage extends UsersBugredAbstractPage {

    @FindBy(xpath = "//*[@id='main-menu']//li/a[contains(@href, '/login')]")
    private WebElement enterButton;

    @FindBy(xpath = "//li[@id='fat-menu']/a[@class='dropdown-toggle']")
    private WebElement userLabel;

    public UsersBugredIndexPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open("/");
    }

    public void clickEnterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(enterButton)).click();
    }

    public String getUserLabelText() {
        return wait.until(ExpectedConditions.visibilityOf(userLabel)).getText();
    }
}
