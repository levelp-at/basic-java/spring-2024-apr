package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import java.math.BigDecimal;

public interface TaxCalculator {

    BigDecimal calculateTax(BigDecimal amount);
}
