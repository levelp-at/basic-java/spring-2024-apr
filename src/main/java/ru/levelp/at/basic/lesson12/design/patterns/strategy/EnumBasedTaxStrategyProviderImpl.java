package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import java.math.BigDecimal;
import ru.levelp.at.basic.lesson12.design.patterns.strategy.dictionary.CurrencyDictionary;

public class EnumBasedTaxStrategyProviderImpl implements TaxStrategyProvider {

    private final TaxCalculator calculator;

    public EnumBasedTaxStrategyProviderImpl(CurrencyDictionary currency) {
        this.calculator = switch (currency) {
            case RUB -> new RubTaxCalculatorImpl();
            case TRY -> new TryTaxCalculatorImpl();
        };
    }

    @Override
    public BigDecimal calculate(BigDecimal amount) {
        return calculator.calculateTax(amount);
    }
}
