package ru.levelp.at.basic.lesson12.design.patterns.factory.method;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import ru.levelp.at.basic.lesson12.design.patterns.factory.method.exception.UnsupportedBrowserException;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class WebDriverFactory {

    private static final String CHROME_BROWSER_NAME = "chrome";
    private static final String FIREFOX_BROWSER_NAME = "firefox";
    private static final String EDGE_BROWSER_NAME = "edge";

    public static WebDriver create(final String browserName) {
        WebDriver driver;
        if (CHROME_BROWSER_NAME.equalsIgnoreCase(browserName)) {
            driver = createChromeDriver();
        } else if (FIREFOX_BROWSER_NAME.equalsIgnoreCase(browserName)) {
            driver = createFirefoxDriver();
        } else if (EDGE_BROWSER_NAME.equalsIgnoreCase(browserName)) {
            driver = createEdgeDriver();
        } else {
            throw new UnsupportedBrowserException(browserName);
        }

        return driver;
    }

    private static WebDriver createChromeDriver() {
        return new ChromeDriver();
    }

    private static WebDriver createFirefoxDriver() {
        return new FirefoxDriver();
    }

    private static WebDriver createEdgeDriver() {
        return new EdgeDriver();
    }
}
