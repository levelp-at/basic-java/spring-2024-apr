package ru.levelp.at.basic.lesson12.design.patterns.builder;

import java.time.LocalDate;

public class PersonWithoutBuilder {

    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final LocalDate birthDate;

    public PersonWithoutBuilder(String firstName, String lastName, String middleName, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthDate = birthDate;
    }

    public PersonWithoutBuilder(String firstName, String lastName, LocalDate birthDate) {
        this(firstName, lastName, "", birthDate);
    }

    //    public PersonWithoutBuilder(String firstName, String middleName, LocalDate birthDate) {
    //        this(firstName, "", middleName, birthDate);
    //    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    @Override
    public String toString() {
        return "PersonWithoutBuilder{"
            + "firstName='" + firstName + '\''
            + ", lastName='" + lastName + '\''
            + ", middleName='" + middleName + '\''
            + ", birthDate=" + birthDate
            + '}';
    }
}
