package ru.levelp.at.basic.lesson12.design.patterns.factory.method.exception;

public class UnsupportedBrowserException extends RuntimeException {

    public UnsupportedBrowserException(String browserName) {
        super("Unsupported browser: %s".formatted(browserName));
    }
}
