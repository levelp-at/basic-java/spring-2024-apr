package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import java.math.BigDecimal;

public class DirectTaxStrategyProviderImpl implements TaxStrategyProvider {

    private final TaxCalculator calculator;

    public DirectTaxStrategyProviderImpl(TaxCalculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public BigDecimal calculate(BigDecimal amount) {
        return calculator.calculateTax(amount);
    }
}
