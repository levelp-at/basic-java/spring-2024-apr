package ru.levelp.at.basic.lesson12.design.patterns.factory.method;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public final class WebDriverSingleton {

    private static WebDriver driver;

    private WebDriverSingleton() {

    }

    public static WebDriver getDriver() {
        if (driver == null) {
            var browserName = System.getProperty("browser.name");
            driver = WebDriverFactory.create(browserName);
        }
        return driver;
    }

    public static void close() {
        driver.quit();
        driver = null;
    }
}
