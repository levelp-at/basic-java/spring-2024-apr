package ru.levelp.at.basic.lesson12.design.patterns.builder;

import java.time.LocalDate;

public class PersonWithBuilder {

    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final LocalDate birthDate;

    public PersonWithBuilder(String firstName, String lastName, String middleName, LocalDate birthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private String firstName;
        private String lastName;
        private String middleName;
        private LocalDate birthDate;

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public Builder middleName(String middleName) {
            this.middleName = middleName;
            return this;
        }

        public Builder birthDate(LocalDate birthDate) {
            this.birthDate = birthDate;
            return this;
        }

        public PersonWithBuilder build() {
            return new PersonWithBuilder(firstName, lastName, middleName, birthDate);
        }
    }

    @Override
    public String toString() {
        return "PersonWithoutBuilder{"
            + "firstName='" + firstName + '\''
            + ", lastName='" + lastName + '\''
            + ", middleName='" + middleName + '\''
            + ", birthDate=" + birthDate
            + '}';
    }
}
