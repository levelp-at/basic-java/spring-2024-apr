package ru.levelp.at.basic.lesson12.design.patterns.factory.method.wildberries;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WildberriesProductPage extends AbstractWildberriesPage {

    public WildberriesProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//p[@class = 'price-block__price-wrap ']//span[@class = 'price-block__wallet-price']")
    private WebElement priceWithWbAccountLabel;

    @FindBy(xpath = "//p[@class = 'price-block__price-wrap ']//del")
    private WebElement priceWithoutWbAccountLabel;

    @Override
    public void open() {
        throw new UnsupportedOperationException();
    }

    public String getPriceWithWbAccountLabel() {
        return wait.until(ExpectedConditions.visibilityOf(priceWithWbAccountLabel)).getText();
    }

    public String getPriceWithoutWbAccountLabel() {
        return wait.until(ExpectedConditions.visibilityOf(priceWithoutWbAccountLabel)).getText();
    }
}
