package ru.levelp.at.basic.lesson12.design.patterns.strategy;

import java.math.BigDecimal;

public class TryTaxCalculatorImpl implements TaxCalculator {

    private static final BigDecimal LOW_TAX_RATE = new BigDecimal("0.15");
    private static final BigDecimal TAX_RATE_MORE_110000_LESS_230000 = new BigDecimal("0.20");
    private static final BigDecimal TAX_RATE_MORE_230000_LESS_870000 = new BigDecimal("0.27");
    private static final BigDecimal TAX_RATE_MORE_870000_LESS_3000000 = new BigDecimal("0.35");
    private static final BigDecimal HIGH_TAX_RATE = new BigDecimal("0.40");

    private static final BigDecimal LIMIT_110000 = new BigDecimal("110000");
    private static final BigDecimal LIMIT_230000 = new BigDecimal("230000");
    private static final BigDecimal LIMIT_870000 = new BigDecimal("870000");
    private static final BigDecimal LIMIT_3000000 = new BigDecimal("3000000");

    @Override
    public BigDecimal calculateTax(BigDecimal amount) {
        if (LIMIT_3000000.compareTo(amount) < 0) {
            return amount.multiply(HIGH_TAX_RATE);
        } else if (LIMIT_3000000.compareTo(amount) >= 0 && LIMIT_870000.compareTo(amount) < 0) {
            return amount.multiply(TAX_RATE_MORE_870000_LESS_3000000);
        } else if (LIMIT_870000.compareTo(amount) >= 0 && LIMIT_230000.compareTo(amount) < 0) {
            return amount.multiply(TAX_RATE_MORE_230000_LESS_870000);
        } else if (LIMIT_230000.compareTo(amount) >= 0 && LIMIT_110000.compareTo(amount) < 0) {
            return amount.multiply(TAX_RATE_MORE_110000_LESS_230000);
        }

        return amount.multiply(LOW_TAX_RATE);
    }
}
