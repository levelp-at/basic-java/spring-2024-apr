package ru.levelp.at.basic.lesson12.design.patterns.builder;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonWithLombokBuilder {

    private final String firstName;
    private final String lastName;
    private final String middleName;
    private final LocalDate birthDate;
}
