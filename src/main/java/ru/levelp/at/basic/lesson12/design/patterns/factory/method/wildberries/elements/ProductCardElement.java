package ru.levelp.at.basic.lesson12.design.patterns.factory.method.wildberries.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ProductCardElement extends AbstractElement {

    private static final By PRICE_WITH_WB_ACCOUNT_XPATH = By
        .xpath(".//p[@class = 'product-card__price price']//ins");
    private static final By PRICE_WITHOUT_WB_ACCOUNT_XPATH = By
        .xpath(".//p[@class = 'product-card__price price']//del");

    private final WebElement root;

    public ProductCardElement(WebDriver driver, WebElement root) {
        super(driver);

        this.root = root;
    }

    public String getPriceWithWbAccount() {
        var price = wait.until(ExpectedConditions
            .presenceOfNestedElementLocatedBy(root, PRICE_WITH_WB_ACCOUNT_XPATH));
        return price.getText();
    }

    public String getPriceWithoutWbAccount() {
        var price = wait.until(ExpectedConditions
            .presenceOfNestedElementLocatedBy(root, PRICE_WITHOUT_WB_ACCOUNT_XPATH));
        return price.getText();
    }

    public void click() {
        wait.until(ExpectedConditions.elementToBeClickable(root)).click();
    }
}
