package ru.levelp.at.basic.utils;

public final class SleepUtil {

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private SleepUtil() {
    }
}
