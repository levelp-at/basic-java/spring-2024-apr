package ru.levelp.at.basic.lesson03;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

public class LetterRemoverImpl implements LetterRemover {

    @Override
    public List<String> remove(List<String> words, String letter) {
        if (CollectionUtils.isEmpty(words)) {
            throw new IllegalArgumentException("words is empty");
        }

        if (StringUtils.isEmpty(letter) || StringUtils.isBlank(letter)) {
            throw new IllegalArgumentException("letter is empty");
        }

        return words.stream()
                    .map(word -> word.replace(letter, ""))
                    .toList();
    }

    @Override
    public List<String> removeNonStream(List<String> words, String letter) {
        if (CollectionUtils.isEmpty(words)) {
            throw new IllegalArgumentException("words is empty");
        }

        if (StringUtils.isEmpty(letter) || StringUtils.isBlank(letter)) {
            throw new IllegalArgumentException("letter is empty");
        }

        List<String> result = new ArrayList<>();

        for (String word : words) {
            String rst = word.replace(letter, "");
            result.add(rst);
        }

        return result;
    }
}
