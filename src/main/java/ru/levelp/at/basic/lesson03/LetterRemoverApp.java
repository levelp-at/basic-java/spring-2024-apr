package ru.levelp.at.basic.lesson03;

import java.util.List;

public class LetterRemoverApp {

    public static void main(String[] args) {
        final var words = List.of("mama", "papa", "silver");
        final var letter = "a";

        final LetterRemover remover = new LetterRemoverImpl();
        System.out.println(remover.remove(words, letter));
        System.out.println(remover.removeNonStream(words, letter));
    }
}
