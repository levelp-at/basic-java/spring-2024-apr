package ru.levelp.at.basic.lesson03;

import java.util.List;

public interface LetterRemover {

    List<String> remove(List<String> words, String letter);

    List<String> removeNonStream(List<String> words, String letter);
}
