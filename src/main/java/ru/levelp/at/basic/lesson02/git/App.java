package ru.levelp.at.basic.lesson02.git;

public class App {

    public static void main(String[] args) {
        final var calculator = new CalculatorImpl();
        System.out.println(String.format("2 + 2 = %d", calculator.add(2, 2)));
        System.out.println(String.format("2 - 2 = %d", calculator.subtract(2, 2)));
        System.out.println(String.format("1 / 3 = %f", calculator.divide(1, 3)));
        System.out.println(String.format("2 / 8 = %f", calculator.divide(2, 8)));
        System.out.println(String.format("1 / 3 = %f", calculator.divide(1, 3)));
        System.out.println(String.format("2 / 8 = %f", calculator.divide(2, 8)));
        System.out.println(String.format("1024 / 8 = %f", calculator.divide(1024, 8)));
        System.out.println(String.format("1 / 3 = %f", calculator.divide(1.0, 3.0, 2)));
        System.out.println(String.format("1 / 6 = %f", calculator.divide(1.0, 6.0, 3)));
        System.out.println(String.format("1 / 3 = %f", calculator.divide(1.0, 3.0, 2)));
        System.out.println(String.format("1 / 6 = %f", calculator.divide(1.0, 6.0, 3)));
        System.out.println(String.format("2 ^ 0 = %d", calculator.power(2, 0)));
        System.out.println(String.format("2 ^ 10 = %d", calculator.power(2, 10)));
    }
}
