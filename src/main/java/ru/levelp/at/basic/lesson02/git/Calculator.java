package ru.levelp.at.basic.lesson02.git;

public interface Calculator {

    int add(int a, int b);

    int subtract(int a, int b);

    int multiply(int a, int b);

    double divide(int a, int b);

    double divide(double a, double b, int accuracy);

    int power(int a, int power);
}
