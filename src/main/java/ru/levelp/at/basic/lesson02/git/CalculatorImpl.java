package ru.levelp.at.basic.lesson02.git;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CalculatorImpl implements Calculator {

    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int subtract(int a, int b) {
        return a - b;
    }

    @Override
    public int multiply(int a, int b) {
        return a * b;
    }

    @Override
    public double divide(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("b is zero");
        }

        return (double) a / b;
    }

    @Override
    public double divide(double a, double b, int accuracy) {
        if (b == 0) {
            throw new IllegalArgumentException("b is zero");
        }

        var aa = BigDecimal.valueOf(a);
        var bb = BigDecimal.valueOf(b);

        return aa.divide(bb, accuracy, RoundingMode.HALF_UP).doubleValue();
    }

    @Override
    public int power(int a, int power) {
        if (power < 0) {
            throw new IllegalArgumentException("b less then zero");
        }

        int result = 1;
        for (int i = 0; i < power; i++) {
            result *= a;
        }

        return result;
    }
}
