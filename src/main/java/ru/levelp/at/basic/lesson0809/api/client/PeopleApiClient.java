package ru.levelp.at.basic.lesson0809.api.client;

import static io.restassured.RestAssured.given;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import java.util.Collections;
import java.util.Map;
import ru.levelp.at.basic.lesson0809.api.model.CreatePersonRequest;

public class PeopleApiClient {

    private static final String PEOPLE_ENDPOINT = "/people";

    private final RequestSpecification requestSpecification;

    public PeopleApiClient(RequestSpecification requestSpecification) {
        this.requestSpecification = requestSpecification;
    }

    public Response createPerson(CreatePersonRequest request) {
        return given()
            .spec(requestSpecification)
            .body(request)
            .when()
            .post(PEOPLE_ENDPOINT);
    }

    public Response getPeople() {
        return getPeople(Collections.emptyMap());
    }

    public Response getPeople(Map<String, Object> queryParameters) {
        return given()
            .spec(requestSpecification)
            .queryParams(queryParameters)
            .when()
            .get(PEOPLE_ENDPOINT);
    }
}
