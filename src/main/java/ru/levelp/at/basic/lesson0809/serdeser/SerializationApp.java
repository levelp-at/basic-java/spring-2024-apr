package ru.levelp.at.basic.lesson0809.serdeser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import ru.levelp.at.basic.lesson0809.serdeser.model.Cat;

public class SerializationApp {

    public static void main(String[] args) {
        final var vaska = new Cat("Vaska", "British", 5);

        try (var fos = new FileOutputStream("fosser.txt"); var oos = new ObjectOutputStream(fos);) {
            oos.writeObject(vaska);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
