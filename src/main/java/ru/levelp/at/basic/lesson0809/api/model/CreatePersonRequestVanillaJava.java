package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.javafaker.Faker;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;

@Data
@Builder
public class CreatePersonRequestVanillaJava {

    private static final Faker FAKER = new Faker();

    @Default
    private final String role = "LECTOR";
    @Default
    private final String email = FAKER.internet().emailAddress();
    @Default
    private final String phoneNumber = FAKER.phoneNumber().cellPhone();
    @Default
    private final String placeOfWork = FAKER.company().name();
    @Default
    private final IdentityData identity = IdentityData.defaultIdentity().build();
    @JsonProperty("address")
    @Default
    private final AddressData addressData = AddressData.defaultAddress().build();
}
