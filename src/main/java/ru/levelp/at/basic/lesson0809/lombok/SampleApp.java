package ru.levelp.at.basic.lesson0809.lombok;

public class SampleApp {

    public static void main(String[] args) {
        final var vaskaJava17 = new CatJava17("1", "11", 111);
        final var vaskaVanillaJava = new CatVanillaJava("2", "22", 222);
        final var vaskaLombok = new CatLombok("3", "33", 333);

        System.out.printf("vaskaJava17 -> %s%n", vaskaJava17);
        System.out.printf("vaskaVanillaJava -> %s%n", vaskaVanillaJava);
        System.out.printf("vaskaLombok -> %s%n", vaskaLombok);

        // Java 17
        System.out.println("Java 17");
        System.out.printf("vaskaJava17.age -> %d%n", vaskaJava17.age());
        System.out.printf("vaskaJava17.name -> %s%n", vaskaJava17.name());
        System.out.printf("vaskaJava17.breed -> %s%n", vaskaJava17.breed());

        // Vanilla Java
        System.out.println("Vanilla Java");
        System.out.printf("vaskaVanillaJava.age -> %d%n", vaskaVanillaJava.getAge());
        System.out.printf("vaskaVanillaJava.name -> %s%n", vaskaVanillaJava.getName());
        System.out.printf("vaskaVanillaJava.breed -> %s%n", vaskaVanillaJava.getBreed());

        // Lombok
        System.out.println("Lombok");
        System.out.printf("vaskaLombok.age -> %d%n", vaskaLombok.getAge());
        System.out.printf("vaskaLombok.name -> %s%n", vaskaLombok.getName());
        System.out.printf("vaskaLombok.breed -> %s%n", vaskaLombok.getBreed());
    }
}
