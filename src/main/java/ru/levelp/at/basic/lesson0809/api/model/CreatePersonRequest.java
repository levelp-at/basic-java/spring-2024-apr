package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.javafaker.Faker;
import lombok.Builder;

@Builder
public record CreatePersonRequest(
    String role,
    String email,
    String phoneNumber,
    String placeOfWork,
    IdentityData identity,
    @JsonProperty("address")
    AddressData addressData
) {

    private static final Faker FAKER = new Faker();

    public static CreatePersonRequestBuilder defaultRequest() {
        return CreatePersonRequest
            .builder()
            .role("WORK_INSPECTOR")
            .email(FAKER.internet().emailAddress())
            .phoneNumber(FAKER.phoneNumber().cellPhone())
            .placeOfWork(FAKER.company().name())
            .identity(IdentityData.defaultIdentity().build())
            .addressData(AddressData.defaultAddress().build());
    }
}
