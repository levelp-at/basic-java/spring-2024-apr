package ru.levelp.at.basic.lesson0809.api.model;

public record PersonResponse(
    PersonData data
) {
}
