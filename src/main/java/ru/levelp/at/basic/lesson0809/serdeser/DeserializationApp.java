package ru.levelp.at.basic.lesson0809.serdeser;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import ru.levelp.at.basic.lesson0809.serdeser.model.Cat;

public class DeserializationApp {

    public static void main(String[] args) {
        try (var fis = new FileInputStream("fosser.txt"); var ois = new ObjectInputStream(fis)) {
            Cat cat = (Cat) ois.readObject();

            System.out.println(cat);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
