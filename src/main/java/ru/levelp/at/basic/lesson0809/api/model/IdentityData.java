package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.github.javafaker.Faker;
import java.time.LocalDate;
import java.time.ZoneId;
import lombok.Builder;

@Builder
public record IdentityData(
    String firstName,
    String lastName,
    String middleName,
    String gender,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    LocalDate dateOfBirth,
    String placeOfBirth,
    PassportData passport
) {

    private static final Faker FAKER = new Faker();

    public static IdentityDataBuilder defaultIdentity() {
        return IdentityData
            .builder()
            .firstName(FAKER.name().firstName())
            .lastName(FAKER.name().lastName())
            .middleName(FAKER.name().username())
            .gender("MALE")
            .dateOfBirth(FAKER.date().birthday().toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .placeOfBirth(FAKER.address().cityName())
            .passport(PassportData.defaultPassport().build());
    }
}
