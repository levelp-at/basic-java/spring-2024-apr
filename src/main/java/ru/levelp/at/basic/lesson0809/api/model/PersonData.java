package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public record PersonData(
    String id,
    String role,
    String email,
    String phoneNumber,
    String placeOfWork,
    IdentityData identity,
    @JsonProperty("address")
    AddressData addressData
) {
}
