package ru.levelp.at.basic.lesson0809.lombok;

import java.io.Serializable;

public record CatJava17(
    String name,
    String breed,
    Integer age
) implements Serializable {
}
