package ru.levelp.at.basic.lesson0809.serdeser.model;

import java.io.Serializable;

public record Cat(
    String name,
    String breed,
    Integer age
) implements Serializable {
}
