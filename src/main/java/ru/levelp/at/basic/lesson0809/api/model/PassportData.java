package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import lombok.Builder;

@Builder
public record PassportData(
    String series,
    String number,
    String placeOfIssue,
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    LocalDate dateOfIssue,
    String departmentCode
) {

    public static PassportDataBuilder defaultPassport() {
        return PassportData
            .builder()
            .series("1234")
            .number("444444")
            .placeOfIssue("")
            .dateOfIssue(LocalDate.parse("1980-02-07"))
            .departmentCode("123-456");
    }
}
