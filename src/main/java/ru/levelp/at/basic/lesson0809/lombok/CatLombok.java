package ru.levelp.at.basic.lesson0809.lombok;

import java.io.Serializable;
import lombok.Data;

@Data
public class CatLombok implements Serializable {

    private final String name;
    private final String breed;
    private final Integer age;
}
