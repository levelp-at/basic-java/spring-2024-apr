package ru.levelp.at.basic.lesson0809.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public record PersonListResponse(
    List<PersonData> data
) {
}
