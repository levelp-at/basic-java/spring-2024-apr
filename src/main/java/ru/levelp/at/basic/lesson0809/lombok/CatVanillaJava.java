package ru.levelp.at.basic.lesson0809.lombok;

import java.io.Serializable;
import java.util.Objects;

public class CatVanillaJava implements Serializable {

    private final String name;
    private final String breed;
    private final Integer age;

    public CatVanillaJava(String name, String breed, Integer age) {
        this.name = name;
        this.breed = breed;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getBreed() {
        return breed;
    }

    public Integer getAge() {
        return age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CatVanillaJava that = (CatVanillaJava) o;
        return Objects.equals(name, that.name) && Objects.equals(breed, that.breed) && Objects.equals(age, that.age);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, breed, age);
    }

    @Override
    public String toString() {
        return "CatVanillaJava{"
            + "name='" + name + '\''
            + ", breed='" + breed + '\''
            + ", age=" + age
            + '}';
    }
}
