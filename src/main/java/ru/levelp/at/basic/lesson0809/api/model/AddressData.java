package ru.levelp.at.basic.lesson0809.api.model;

import lombok.Builder;

@Builder
public record AddressData(
    String street,
    Integer houseNumber,
    Integer houseBuilding,
    String houseLetter,
    Integer flat,
    String city,
    String postalCode
) {
    public static AddressDataBuilder defaultAddress() {
        return AddressData
            .builder()
            .street("")
            .city("")
            .flat(12)
            .houseBuilding(33)
            .houseLetter("F")
            .houseNumber(32)
            .postalCode("");
    }
}
