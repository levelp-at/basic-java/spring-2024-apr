package ru.levelp.at.basic.lesson1011.allure.wildberries;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import ru.levelp.at.basic.lesson1011.allure.wildberries.elements.ProductCardElement;

public class WildberriesProductsPage extends AbstractWildberriesPage {

    private static final By ARTICLES_XPATH = By
        .xpath("//div[@class = 'product-card-overflow']//article");

    public WildberriesProductsPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        throw new UnsupportedOperationException();
    }

    public ProductCardElement getProductCard(final int cardIndex) {
        var cards = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(ARTICLES_XPATH, 5));

        // System.out.println("====");
        // System.out.println(cards.get(cardIndex - 1).getText());
        // System.out.println("====");

        return new ProductCardElement(driver, cards.get(cardIndex - 1));
    }
}
