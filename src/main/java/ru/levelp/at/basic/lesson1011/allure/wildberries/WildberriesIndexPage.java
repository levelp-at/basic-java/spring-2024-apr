package ru.levelp.at.basic.lesson1011.allure.wildberries;

import org.openqa.selenium.WebDriver;

public class WildberriesIndexPage extends AbstractWildberriesPage {

    public WildberriesIndexPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open("/");
    }
}
