package ru.levelp.at.basic.lesson1011.allure.wildberries;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.levelp.at.basic.lesson1011.allure.wildberries.elements.HeaderElement;

public abstract class AbstractWildberriesPage {

    private static final String WILDBERRIES_BASE_URL = "https://www.wildberries.ru/";

    protected final WebDriver driver;
    protected final WebDriverWait wait;
    private HeaderElement header;

    protected AbstractWildberriesPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(5000));
        PageFactory.initElements(driver, this);

        this.header = new HeaderElement(driver);
    }

    public abstract void open();

    protected void open(final String relativeUrl) {
        driver.navigate().to("%s%s".formatted(WILDBERRIES_BASE_URL, relativeUrl));
    }

    public HeaderElement header() {
        return header;
    }
}
