package ru.levelp.at.basic.lesson1011.allure.usersbugred;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class UsersBugredAbstractPage {

    private static final String USERS_BUGRED_BASE_URL = "http://users.bugred.ru";

    protected final WebDriver driver;
    protected final WebDriverWait wait;

    protected UsersBugredAbstractPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(4000));

        PageFactory.initElements(driver, this);
    }

    public abstract void open();

    protected void open(final String relativeUrl) {
        driver.navigate().to("%s%s".formatted(USERS_BUGRED_BASE_URL, relativeUrl));
    }

    protected void sendKeysToElement(WebElement textField, String text) {
        wait.until(ExpectedConditions.visibilityOf(textField)).sendKeys(text);
    }
}
