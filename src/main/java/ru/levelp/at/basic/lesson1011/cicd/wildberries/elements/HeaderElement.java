package ru.levelp.at.basic.lesson1011.cicd.wildberries.elements;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class HeaderElement extends AbstractElement {

    private MenuElement menu;

    @FindBy(xpath = "//header//*[@data-wba-header-name = 'Catalog']")
    private WebElement menuElement;

    public HeaderElement(WebDriver driver) {
        super(driver);

        menu = new MenuElement(driver);
    }

    public void openMenu() {
        wait.until(ExpectedConditions.elementToBeClickable(menuElement)).click();
    }

    public MenuElement menu() {
        return menu;
    }
}
