package ru.levelp.at.basic.lesson0507.selenium.page.components;

import org.openqa.selenium.WebDriver;

public class WildberriesIndexPage extends AbstractWildberriesPage {

    public WildberriesIndexPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open("/");
    }
}
