package ru.levelp.at.basic.lesson0507.selenium.page.objects.type.fluent;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UsersBugredLoginRegistrationPage {

    private final WebDriver driver;
    private final WebDriverWait wait;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='name']")
    private WebElement nameTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='email']")
    private WebElement emailTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='act_register_now']")
    private WebElement registerButton;

    @FindBy(xpath = "//form[contains(@action, '/register/')]/p")
    private WebElement errorMessageLabel;

    public UsersBugredLoginRegistrationPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(4000));

        PageFactory.initElements(driver, this);
    }

    public UsersBugredLoginRegistrationPage sendKeysToNameTextField(String name) {
        wait.until(ExpectedConditions.visibilityOf(nameTextField)).sendKeys(name);
        return this;
    }

    public UsersBugredLoginRegistrationPage sendKeysToEmailTextField(String email) {
        wait.until(ExpectedConditions.visibilityOf(emailTextField)).sendKeys(email);
        return this;
    }

    public UsersBugredLoginRegistrationPage sendKeysToPasswordTextField(String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordTextField)).sendKeys(password);
        return this;
    }

    //    public UsersBugredIndexPage clickRegisterButton() {
    //        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
    //        return new UsersBugredIndexPage(driver);
    //    }

    public UsersBugredIndexPage clickSuccessRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
        return new UsersBugredIndexPage(driver);
    }

    public UsersBugredLoginRegistrationPage clickFailRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
        return this;
    }

    public String getErrorMessageText() {
        return wait.until(ExpectedConditions.visibilityOf(errorMessageLabel)).getText();
    }
}
