package ru.levelp.at.basic.lesson0507.selenium.steps.usersbugred;

import io.qameta.allure.Param;
import io.qameta.allure.Step;
import io.qameta.allure.model.Parameter.Mode;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UsersBugredLoginRegistrationPage extends UsersBugredAbstractPage {

    private static final String RELATIVE_URL = "/user/login/index.html";

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='name']")
    private WebElement nameTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='email']")
    private WebElement emailTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='act_register_now']")
    private WebElement registerButton;

    @FindBy(xpath = "//form[contains(@action, '/register/')]/p")
    private WebElement errorMessageLabel;

    public UsersBugredLoginRegistrationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public void open() {
        open(RELATIVE_URL);
    }

    @Step("Вводим имя")
    public void sendKeysToNameTextField(String name) {
        sendKeysToElement(nameTextField, name);
    }

    @Step("Вводим email")
    public void sendKeysToEmailTextField(String email) {
        sendKeysToElement(emailTextField, email);
    }

    @Step("Вводим пароль")
    public void sendKeysToPasswordTextField(@Param(mode = Mode.MASKED) String password) {
        sendKeysToElement(passwordTextField, password);
    }

    public void clickRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
    }

    public String getErrorMessageText() {
        return wait.until(ExpectedConditions.visibilityOf(errorMessageLabel)).getText();
    }
}
