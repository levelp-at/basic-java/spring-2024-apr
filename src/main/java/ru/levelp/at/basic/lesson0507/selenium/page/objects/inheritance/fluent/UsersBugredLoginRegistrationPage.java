package ru.levelp.at.basic.lesson0507.selenium.page.objects.inheritance.fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UsersBugredLoginRegistrationPage extends UsersBugredAbstractPage<UsersBugredLoginRegistrationPage> {

    private static final String RELATIVE_URL = "/user/login/index.html";

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='name']")
    private WebElement nameTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='email']")
    private WebElement emailTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//form[contains(@action, '/register/')]//input[@name='act_register_now']")
    private WebElement registerButton;

    @FindBy(xpath = "//form[contains(@action, '/register/')]/p")
    private WebElement errorMessageLabel;

    public UsersBugredLoginRegistrationPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public UsersBugredLoginRegistrationPage open() {
        open(RELATIVE_URL);
        return this;
    }

    public UsersBugredLoginRegistrationPage sendKeysToNameTextField(String name) {
        wait.until(ExpectedConditions.visibilityOf(nameTextField)).sendKeys(name);
        return this;
    }

    public UsersBugredLoginRegistrationPage sendKeysToEmailTextField(String email) {
        wait.until(ExpectedConditions.visibilityOf(emailTextField)).sendKeys(email);
        return this;
    }

    public UsersBugredLoginRegistrationPage sendKeysToPasswordTextField(String password) {
        wait.until(ExpectedConditions.visibilityOf(passwordTextField)).sendKeys(password);
        return this;
    }

    //    public UsersBugredIndexPage clickRegisterButton() {
    //        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
    //        return new UsersBugredIndexPage(driver);
    //    }

    public UsersBugredIndexPage clickSuccessRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
        return new UsersBugredIndexPage(driver);
    }

    public UsersBugredLoginRegistrationPage clickFailRegisterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(registerButton)).click();
        return this;
    }

    public String getErrorMessageText() {
        return wait.until(ExpectedConditions.visibilityOf(errorMessageLabel)).getText();
    }
}
