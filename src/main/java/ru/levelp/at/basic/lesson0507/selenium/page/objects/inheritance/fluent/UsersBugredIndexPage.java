package ru.levelp.at.basic.lesson0507.selenium.page.objects.inheritance.fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class UsersBugredIndexPage extends UsersBugredAbstractPage<UsersBugredAbstractPage> {

    @FindBy(xpath = "//*[@id='main-menu']//li/a[contains(@href, '/login')]")
    private WebElement enterButton;

    @FindBy(xpath = "//li[@id='fat-menu']/a[@class='dropdown-toggle']")
    private WebElement userLabel;

    public UsersBugredIndexPage(WebDriver driver) {
        super(driver);
    }

    @Override
    public UsersBugredIndexPage open() {
        open("/");
        return this;
    }

    public UsersBugredLoginRegistrationPage clickEnterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(enterButton)).click();
        return new UsersBugredLoginRegistrationPage(driver);
    }

    public String getUserLabelText() {
        return wait.until(ExpectedConditions.visibilityOf(userLabel)).getText();
    }
}
