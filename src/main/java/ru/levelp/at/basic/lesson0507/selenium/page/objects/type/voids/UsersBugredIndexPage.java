package ru.levelp.at.basic.lesson0507.selenium.page.objects.type.voids;

import java.time.Duration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UsersBugredIndexPage {

    private static final String USERS_BUGRED_URL = "http://users.bugred.ru/";

    private final WebDriver driver;
    private final WebDriverWait wait;

    @FindBy(xpath = "//*[@id='main-menu']//li/a[contains(@href, '/login')]")
    private WebElement enterButton;

    @FindBy(xpath = "//li[@id='fat-menu']/a[@class='dropdown-toggle']")
    private WebElement userLabel;

    public UsersBugredIndexPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, Duration.ofMillis(4000));

        PageFactory.initElements(driver, this);
    }

    public void open() {
        this.driver.navigate().to(USERS_BUGRED_URL);
    }

    public void clickEnterButton() {
        wait.until(ExpectedConditions.elementToBeClickable(enterButton)).click();
    }

    public String getUserLabelText() {
        return wait.until(ExpectedConditions.visibilityOf(userLabel)).getText();
    }
}
